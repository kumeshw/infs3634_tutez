package jk3634group.tutez.databasemanager;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by homeworld on 10/21/16.
 */

/*
This is a class designed to provide the basics for the database needs of the system
This class 'DatabaseHelper extends the SQLiteHelper class from Androids API
This will be primarily used for constructing the database as we have the DAO's for CRUD operations
 */

public class DatabaseHelper extends SQLiteOpenHelper{

    //Give the database a version number
    public static final int DATABASE_VERSION = 6;

    //Assign the database name
    public static final String DATABASE_NAME = "tutez.db";

    //Assign the database table names
    //For evert DTO (Data Transfer Object) there must be a table
    public static final String TABLE_STUDENT = "student";
    public static final String TABLE_CLASS = "class";
    public static final String TABLE_ENROLMENT = "enrolment";
    public static final String TABLE_ASSIGNMENTS = "assignments";
    public static final String TABLE_ASSIGNMENTCLASS = "assignmentClass";
    public static final String TABLE_CLASSATTENDENCE = "classAttendence";
    public static final String TABLE_CLASSATTENDENCEHOLDER = "classAttendenceHolder";
    public static final String TABLE_INFORMATIONSENDER = "informationSender";
    public static final String TABLE_RESULTS = "results";
    public static final String TABLE_STUDENTHISTORY = "studentHistory";

        //Table ID column names
        //Some of these will re-ocur throughout other tables
        public static final String STUDENTID = "studentId";
        public static final String CLASSID = "classId";
        public static final String ENROLMENTID = "enrolmentId";
        public static final String ASSIGNMENTID = "assignmentId";
        public static final String ASSIGNMENTCLASSID = "assigmentClassId";
        public static final String CLASSATTENDENCEID = "classAttendenceId";
        public static final String CLASSATTENDENCEHOLDERID = "classAttendanceHolderId";
        public static final String INFORMATIONSENDERID = "informationSenderId";
        public static final String RESULTID = "resultId";
        public static final String STUDENTHISTORYID = "studentHistoryId";

        //Student specific columns
        public static final String FIRSTNAME = "firstName";
        public static final String LASTNAME = "lastName";
        public static final String EMAIL = "email";

        //Class specific columns
        public static final String CLASSCODE = "classCode";
        public static final String CLASSNAME = "className";
        public static final String SEMESTER = "semester";
        public static final String YEAR = "year";

        //Enrolment specific columns: none

        //Assignments specific columns
        public static final String ASSIGNMENTNAME = "assignmentName";
        public static final String ASSIGNMENTDESCRIPTION = "assignmentDescription";

        //AssignmentClass specific culumns: none

        //ClassAttendence specific columns
        public static final String DIDATTEND = "didAttend";

        //InformationSender specific columns
        public static final String TITLE = "title";
        public static final String INFORMATION = "information";

        //Results specific columns
        public static final String MARK = "mark";

        //StudentHistory specific columns
        public static final String DESCRIPTION = "description";
        public static final String CATEGORY = "category";

        //class attendance holder specific columns
        public static final String ATTENDANCERECORD = "attendanceRecord";

    //Table CREATE Statements

    //Student Table
    public static final String CREATE_TABLE_STUDENT="CREATE TABLE " + TABLE_STUDENT
            + " ( "
            + STUDENTID + " INTEGER PRIMARY KEY, " //The student ID has to be unique
            + FIRSTNAME + " VARCHAR(50), " //Students first name
            + LASTNAME + " VARCHAR(50), " //Students last name
            + EMAIL + " VARCHAR(50)" //Students email
            +" )";

    //Enrolment Table
    public static final String CREATE_TABLE_ENROLMENT="CREATE TABLE " + TABLE_ENROLMENT
            + " ( "
            + ENROLMENTID + " INTEGER PRIMARY KEY, " //The enrolment ID has to be unique
            + STUDENTID + " INT(11), " //References foreign student key
            + CLASSID + " INT(11) " //References foreign class key
            +" ) ";

    //Class Table
    public static final String CREATE_TABLE_CLASS="CREATE TABLE " + TABLE_CLASS
            + " ( "
            + CLASSID + " INTEGER PRIMARY KEY, " //The class ID has to be unique
            + CLASSCODE + " VARCHAR(50), " //Class code i.e. INFS3634
            + CLASSNAME + " VARCHAR(50), " //Classes name i.e. Mobile Business Systems
            + SEMESTER + " INT(11), "  // Which semester is the tutor teaching this class
            + YEAR + " INT(11) " // Which year is the tutor teaching this class
            +" ) ";

    //Assignments Table
    public static final String CREATE_TABLE_ASSIGNMENTS="CREATE TABLE " + TABLE_ASSIGNMENTS
            + " ( "
            + ASSIGNMENTID + " INTEGER PRIMARY KEY, " //The assignment ID has to be unique
            + ASSIGNMENTNAME + " VARCHAR(50), " //The name of the assignment i.e. GroupAssignment
            + ASSIGNMENTDESCRIPTION + " VARCHAR(255) " //The description of the assignment
            +" ) ";

    //AssignmentClass Table
    public static final String CREATE_TABLE_ASSIGNMENTCLASS="CREATE TABLE " + TABLE_ASSIGNMENTCLASS
            + " ( "
            + ASSIGNMENTCLASSID + " INTEGER PRIMARY KEY, " //The assignment class ID has to be unique
            + ASSIGNMENTID + " INT(11), " //References the foreign Assignment key
            + CLASSID + " INT(11) " //References the foreign Class key
            +") ";

    //class attendence holder
    public static final String CREATE_TABLE_CLASSATTENDANCEHOLDER="CREATE TABLE " + TABLE_CLASSATTENDENCEHOLDER
            + " ( "
            + CLASSATTENDENCEHOLDERID + " INTEGER PRIMARY KEY, " //The class classHolder ID has to be unique
            + ATTENDANCERECORD + " VARCHAR(50), "
            + CLASSID + " INT(11) " //References the foreign Class key
            +" ) ";

    //InformationSender Table
    public static final String CREATE_TABLE_INFORMATIONSENDER="CREATE TABLE " + TABLE_INFORMATIONSENDER
            + " ( "
            + INFORMATIONSENDERID + " INTEGER PRIMARY KEY, " //The class informationSender ID has to be unique
            + CLASSID + " INT(11), " //References the foreign Class key
            + TITLE + " VARCHAR(50), " //Title of information to be sent
            + INFORMATION + " VARCHAR(255) " //Information to send
            +" ) ";

    //ClassAttendence Table
    public static final String CREATE_TABLE_CLASSATTENDENCE="CREATE TABLE " + TABLE_CLASSATTENDENCE
            + " ( "
            + CLASSATTENDENCEID + " INTEGER PRIMARY KEY, " //The class attendence ID has to be unique
            + CLASSID + " INT(11), " //References the foreign Class key
            + STUDENTID + " INT(11), " //References the foreign Student key
            + CLASSATTENDENCEHOLDERID + " INT(11), " //References the foreign Student key
            + DIDATTEND + " INT(5) " //Emulates a Boolean of 1=yes they did and 0 = no they did not
            +" )";

    //Results Table
    public static final String CREATE_TABLE_RESULTS="CREATE TABLE " + TABLE_RESULTS
            + " ( "
            + RESULTID + " INTEGER PRIMARY KEY, " //The student ID has to be unique
            + STUDENTID + " INT(11), " //References the foreign Student key
            + ASSIGNMENTID + " INT(11), " //References the foreign Assignment key
            + MARK + " DOUBLE(100) " //The mark the student received for the course
            +" ) ";

    //StudentHistory Table
    public static final String CREATE_TABLE_STUDENTHISTORY="CREATE TABLE " + TABLE_STUDENTHISTORY
            + " ( "
            + STUDENTHISTORYID + " INTEGER PRIMARY KEY, " //The student history ID has to be unique
            + STUDENTID + " INT(11), " //References the foreign Student key
            + CATEGORY + " VARCHAR(50), " //The category of the history incedent i.e. acedemic offense
            + DESCRIPTION + " VARCHAR(255) " //The specific details of the offense
            +")";


    //This is the constructor for this class
    public DatabaseHelper(Context context) {

        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        //context.deleteDatabase(DATABASE_NAME);
    }

    //When this is run
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        //Create required tables

        sqLiteDatabase.execSQL(CREATE_TABLE_STUDENT);
        sqLiteDatabase.execSQL(CREATE_TABLE_CLASS);
        sqLiteDatabase.execSQL(CREATE_TABLE_ENROLMENT);
        sqLiteDatabase.execSQL(CREATE_TABLE_CLASSATTENDENCE);
        sqLiteDatabase.execSQL(CREATE_TABLE_ASSIGNMENTS);
        sqLiteDatabase.execSQL(CREATE_TABLE_ASSIGNMENTCLASS);
        sqLiteDatabase.execSQL(CREATE_TABLE_RESULTS);
        sqLiteDatabase.execSQL(CREATE_TABLE_STUDENTHISTORY);
        sqLiteDatabase.execSQL(CREATE_TABLE_INFORMATIONSENDER);
        sqLiteDatabase.execSQL(CREATE_TABLE_CLASSATTENDANCEHOLDER);
    }

    //Method to upgrade the versions of the database should we add columns
    //please don't add columns, they scare me...
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        //TODO
        if(i1>i){

        }
    }

    //Override the onOpenClass
    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
    }

    //Lets make an 'instance' of this database helper
    private static DatabaseHelper instance;

    //For the DAOs (Data Access Objects) we will make a getHelper method
    //this is so we can manipulate data from the other classes to make
    //data accessing simple and easy
    public static synchronized DatabaseHelper getHelper(Context context) {
        if (instance == null)
            instance = new DatabaseHelper(context);
        return instance;
    }


}
