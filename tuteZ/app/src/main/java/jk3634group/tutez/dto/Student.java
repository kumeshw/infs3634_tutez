package jk3634group.tutez.dto;

import java.io.Serializable;

/**
 * Created by homeworld on 10/21/16.
 */

//This is a data transfer object for Student information
//This will hold information on each student
//This would be the equivalent of a Model.
//Needs to implement serializable inorder to pass objects through intents
public class Student implements Serializable {

    //Define private variables
    private int studentId;
    private String firstName;
    private String lastName;
    private String email;


    //Define getters and setters
    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


}
