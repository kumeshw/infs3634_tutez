package jk3634group.tutez.dto;

import java.io.Serializable;

/**
 * Created by homeworld on 10/21/16.
 */

//This class is used to track which student has attended which class
//Needs to implement serializable inorder to pass objects through intents
public class ClassAttendence implements Serializable {

    //Define private variables
    private int classAttendenceId;
    private int classId;
    private int studentId;
    private int didAttend;
    private int classAttendanceHolderId;

    //Define getters and setters
    public int getClassAttendenceId() {
        return classAttendenceId;
    }

    public void setClassAttendenceId(int classAttendenceId) {
        this.classAttendenceId = classAttendenceId;
    }

    public int getClassId() {
        return classId;
    }

    public void setClassId(int classId) {
        this.classId = classId;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public int getDidAttend() {
        return didAttend;
    }

    public void setDidAttend(int didAttend) {
        this.didAttend = didAttend;
    }

    public int getClassAttendanceHolderId() {
        return classAttendanceHolderId;
    }

    public void setClassAttendanceHolderId(int classAttendanceHolderId) {
        this.classAttendanceHolderId = classAttendanceHolderId;
    }
}
