package jk3634group.tutez.dto;

import java.io.Serializable;

/**
 * Created by homeworld on 10/21/16.
 */

//This class is used to assign assignments to classes
//Needs to implement serializable to pass objects thorugh intents
public class AssignmentClass implements Serializable {

    //Define private variables
    private int assignmentClassId;
    private int assignmentId;
    private int classId;

    //Define getters and setters
    public int getAssignmentClassId() {
        return assignmentClassId;
    }

    public void setAssignmentClassId(int assignmentClassId) {
        this.assignmentClassId = assignmentClassId;
    }

    public int getAssignmentId() {
        return assignmentId;
    }

    public void setAssignmentId(int assignmentId) {
        this.assignmentId = assignmentId;
    }

    public int getClassId() {
        return classId;
    }

    public void setClassId(int classId) {
        this.classId = classId;
    }
}
