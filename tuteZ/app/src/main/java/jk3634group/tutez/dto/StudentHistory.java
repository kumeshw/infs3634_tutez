package jk3634group.tutez.dto;

import java.io.Serializable;

/**
 * Created by homeworld on 10/21/16.
 */

//This is a class to define various items that occur
//if such a item is a special interest - such as student used a swear word in class
//Needs to implement serializable inorder to pass objects through intents
public class StudentHistory implements Serializable {

    //Define private variables
    private int studentHistoryId;
    private int studentId;
    private String description;
    private String category;

    //Define getters and setters
    public int getStudentHistoryId() {
        return studentHistoryId;
    }

    public void setStudentHistoryId(int studentHistoryId) {
        this.studentHistoryId = studentHistoryId;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
