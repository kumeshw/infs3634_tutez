package jk3634group.tutez.dto;

import java.io.Serializable;

/**
 * Created by homeworld on 10/21/16.
 */

//This is a class that holds information about class specific assignments
//Needs to implement serializable inorder to pass objects thorugh intents
public class Assignments implements Serializable{

    //Define privare variables
    private int assignmentId;
    private String assignmentName;
    private String assignmentDescription;

    //Define getters and setters
    public int getAssignmentId() {
        return assignmentId;
    }

    public void setAssignmentId(int assignmentId) {
        this.assignmentId = assignmentId;
    }

    public String getAssignmentName() {
        return assignmentName;
    }

    public void setAssignmentName(String assignmentName) {
        this.assignmentName = assignmentName;
    }

    public String getAssignmentDescription() {
        return assignmentDescription;
    }

    public void setAssignmentDescription(String assignmentDescription) {
        this.assignmentDescription = assignmentDescription;
    }
}
