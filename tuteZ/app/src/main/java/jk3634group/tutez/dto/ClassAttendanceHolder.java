package jk3634group.tutez.dto;

import java.io.Serializable;

/**
 * Created by homeworld on 10/24/16.
 */

public class ClassAttendanceHolder implements Serializable{
    private int classAttendanceHolderId;
    private String attendanceRecord;
    private int classId;

    public int getClassAttendanceHolderId() {
        return classAttendanceHolderId;
    }

    public void setClassAttendanceHolderId(int classAttendanceHolderId) {
        this.classAttendanceHolderId = classAttendanceHolderId;
    }

    public String getAttendanceRecord() {
        return attendanceRecord;
    }

    public void setAttendanceRecord(String attendanceRecord) {
        this.attendanceRecord = attendanceRecord;
    }

    public int getClassId() {
        return classId;
    }

    public void setClassId(int classId) {
        this.classId = classId;
    }
}
