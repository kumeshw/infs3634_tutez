package jk3634group.tutez.dto;

import java.io.Serializable;

/**
 * Created by homeworld on 10/21/16.
 */

//This is to contain the information that the tutor may wish to send to students
//Needs to implement serializable inorder to pass information through intents
public class InformationSender implements Serializable {

    //Define private variables
    private int informationSenderId;
    private int classId;
    private String title;
    private String information;

    //Define getters and setters
    public int getInformationSenderId() {
        return informationSenderId;
    }

    public void setInformationSenderId(int informationSenderId) {
        this.informationSenderId = informationSenderId;
    }

    public int getClassId() {
        return classId;
    }

    public void setClassId(int classId) {
        this.classId = classId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getInformation() {
        return information;
    }

    public void setInformation(String information) {
        this.information = information;
    }
}
