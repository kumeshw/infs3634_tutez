package jk3634group.tutez.dto;

import java.io.Serializable;

/**
 * Created by homeworld on 10/21/16.
 */

//This class assigns students to classes
//Needs to implement serializable inorder to pass objects through intents
public class Enrolment implements Serializable {

    //Define private variables
    private int EnrolmentId;
    private int studentId;
    private int classId;

    //Define getters and setters
    public int getEnrolmentId() {
        return EnrolmentId;
    }

    public void setEnrolmentId(int enrolmentId) {
        EnrolmentId = enrolmentId;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public int getClassId() {
        return classId;
    }

    public void setClassId(int classId) {
        this.classId = classId;
    }
}
