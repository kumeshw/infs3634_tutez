package jk3634group.tutez.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.sql.SQLException;
import java.util.ArrayList;

import jk3634group.tutez.dto.ClassAttendence;

/**
 * Created by homeworld on 10/21/16.
 */

//This class is a data access object for ClassAttendence.
//The primary purpose for this class is to provide all the necessary database CRUD operations
//this improves ease of use in other code
// ---- this was also the quickest way to make large amounts of functionality's

//Must extend the BaseDAO for database accessing needs
public class ClassAttendenceDAO extends BaseDAO {


    public ClassAttendenceDAO(Context context) {
        super(context);
    }


    //Simple select based on Class ID
    public ArrayList<ClassAttendence> getAttendenceByClassID(int id){
        //Create a returnable ArrayList
        //This will hold all the objest for all the assignments
        ArrayList<ClassAttendence> classAttendences = new ArrayList<ClassAttendence>();

        //Get the Database to operate on
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM classAttendence WHERE classId ="+id, null);



        //Get an assignment from this cursor
        while (cur.moveToNext()) {

            //Attempt to assign assignment
            try {
                ClassAttendence classAttendence = null;
                classAttendence = parseClassAttendence(cur);
                classAttendences.add(classAttendence);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return classAttendences;
    }

    //Simple select based on Class ID
    public ArrayList<ClassAttendence> getAttendenceByClassAttendanceHolderID(int id){
        //Create a returnable ArrayList
        //This will hold all the objest for all the assignments
        ArrayList<ClassAttendence> classAttendences = new ArrayList<ClassAttendence>();

        //Get the Database to operate on
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM classAttendence WHERE classAttendanceHolderId ="+id, null);



        //Get an assignment from this cursor
        while (cur.moveToNext()) {

            //Attempt to assign assignment
            try {
                ClassAttendence classAttendence = null;
                classAttendence = parseClassAttendence(cur);
                classAttendences.add(classAttendence);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return classAttendences;
    }

    public ClassAttendence getAttendenceByTwo (int studentId, int classId){
        //Create a returnable ArrayList
        //This will hold all the objest for all the assignments
        ArrayList<ClassAttendence> classAttendences = new ArrayList<ClassAttendence>();

        //Get the Database to operate on
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM classAttendence WHERE classId ="+classId + " AND studentId =" +studentId, null);
        ClassAttendence classAttendence = new ClassAttendence();


        //Get an assignment from this cursor
        while (cur.moveToNext()) {

            //Attempt to assign assignment
            try {

                classAttendence = parseClassAttendence(cur);

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return classAttendence;
    }

    //Simple select based on Class ID
    public boolean checkForAttendance(int classId, int studentId){
        //Create a returnable ArrayList
        //This will hold all the objest for all the assignments

        //Get the Database to operate on
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM classAttendence WHERE classId ="+classId + " AND studentId ="+studentId, null);
        boolean b = false;


        //Get an assignment from this cursor
        while (cur.moveToNext()) {

            //Attempt to assign assignment
            try {
                ClassAttendence classAttendence = null;
                classAttendence = parseClassAttendence(cur);
                b = true;
            } catch (SQLException e) {
                e.printStackTrace();

            }
        }
        return b;
    }

    //Simple select based on Student ID
    public ArrayList<ClassAttendence> getAttendenceByStudentID(int id){
        //Create a returnable ArrayList
        //This will hold all the objest for all the assignments
        ArrayList<ClassAttendence> classAttendences = new ArrayList<ClassAttendence>();

        //Get the Database to operate on
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM classAttendence WHERE studentId ="+id, null);



        //Get an assignment from this cursor
        while (cur.moveToNext()) {

            //Attempt to assign assignment
            try {
                ClassAttendence classAttendence = null;
                classAttendence = parseClassAttendence(cur);
                classAttendences.add(classAttendence);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return classAttendences;
    }

    //Simple select based on Student ID
    public ArrayList<ClassAttendence> getAttendenceByClassHolderID(int id){
        //Create a returnable ArrayList
        //This will hold all the objest for all the assignments
        ArrayList<ClassAttendence> classAttendences = new ArrayList<ClassAttendence>();

        //Get the Database to operate on
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM classAttendence WHERE classAttendanceHolderId ="+id, null);



        //Get an assignment from this cursor
        while (cur.moveToNext()) {

            //Attempt to assign assignment
            try {
                ClassAttendence classAttendence = null;
                classAttendence = parseClassAttendence(cur);
                classAttendences.add(classAttendence);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return classAttendences;
    }

    //Determine if ClassAttendence needs to be added or updated
    public void addOrUpdate(ClassAttendence classAttendence)  {
        if (classAttendence.getClassAttendenceId() > 0) {
            update(classAttendence);
        } else {
            add(classAttendence);
        }
    }

    //Add a class attendence record to the database
    //This method adds a class attendence into the database
    public boolean add(ClassAttendence classAttendence){
        //Get the Database to operate on
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        //Use content values to add to the database
        ContentValues contentValues = new ContentValues();

        //Try to apply this variable
        try{
            //get the values and allign them to their table
            contentValues = fillContentValues(contentValues, classAttendence);
            db.insert(databaseHelper.TABLE_CLASSATTENDENCE, null, contentValues);
            return true;
        }catch(Exception e){
            e.getMessage();
            return false;
        }

    }


    //update student in the database
    public boolean update(ClassAttendence classAttendence){
        //Get the Database to operate on
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        //Use content values to add to the database
        ContentValues contentValues = null;
        int id = classAttendence.getClassAttendenceId();
        String where = "classAttendendenceId=?";
        String[] whereArgs = new String[] {String.valueOf(id)};
        //Try to apply this variable
        try{
            //get the values and allign them to their table
            contentValues = fillContentValues(contentValues, classAttendence);
            db.update(databaseHelper.TABLE_CLASSATTENDENCE, contentValues, where, whereArgs);
            return true;
        }catch(Exception e){
            e.getMessage();
            return false;
        }
    }

    //I cant see these being deleted by for now ill only delete based on class id for then sake of
    //removing a classes presence on a system
    public boolean deleteByClassId(int id){
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        try {
            db.execSQL("DELETE FROM " + databaseHelper.TABLE_CLASSATTENDENCE + " WHERE classId='" + id + "'");
            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }

    //I cant see these being deleted by for now ill only delete based on class id for then sake of
    //removing a classes presence on a system
    public boolean deleteAll(){
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        try {
            db.execSQL("DELETE FROM " + databaseHelper.TABLE_CLASSATTENDENCE);
            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }

    //ID => classId => studentId => didAttend
    //This method creates a InforamtionSender from a ResultSet.
    //This is helpful for generating large amounts of objects easilt
    public static ClassAttendence parseClassAttendence(Cursor cur) throws SQLException {
        //Create a new Class
        ClassAttendence classAttendence = new ClassAttendence();

        //Define its variables
        classAttendence.setClassAttendenceId(cur.getInt(cur.getColumnIndex("classAttendenceId")));
        classAttendence.setClassId(cur.getInt(cur.getColumnIndex("classId")));
        classAttendence.setStudentId(cur.getInt(cur.getColumnIndex("studentId")));
        classAttendence.setDidAttend(cur.getInt(cur.getColumnIndex("didAttend")));
        classAttendence.setClassAttendanceHolderId(cur.getInt(cur.getColumnIndex("classAttendanceHolderId")));
        //Return this Class to the caller
        return classAttendence;
    }

    //This is another helper method
    //This one helps fill a Content values
    public ContentValues fillContentValues(ContentValues contentValues, ClassAttendence classAttendence) throws SQLException {
        contentValues.put(databaseHelper.CLASSID, classAttendence.getClassId());
        contentValues.put(databaseHelper.STUDENTID, classAttendence.getStudentId());
        contentValues.put(databaseHelper.DIDATTEND, classAttendence.getDidAttend());
        contentValues.put(databaseHelper.CLASSATTENDENCEHOLDERID, classAttendence.getClassAttendanceHolderId());
        return contentValues;
    }
}
