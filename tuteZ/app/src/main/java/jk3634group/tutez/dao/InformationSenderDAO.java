package jk3634group.tutez.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import jk3634group.tutez.dto.InformationSender;

/**
 * Created by homeworld on 10/21/16.
 */

//This class is a data access object for InformationSender.
//The primary purpose for this class is to provide all the necessary database CRUD operations
//this improves ease of use in other code
// ---- this was also the quickest way to make large amounts of functionality's

//Must extend the BaseDAO for database accessing needs
public class InformationSenderDAO extends BaseDAO {


    public InformationSenderDAO(Context context) {
        super(context);
    }
    
    //For now we are just saving information sender
    //Determine if Assignment needs to be added or updated
    public void addOrUpdate(InformationSender informationSender)  {
        if (informationSender.getInformationSenderId() > 0) {
            update(informationSender);
        } else {
            add(informationSender);
        }
    }

    //Add an informationSender record to the database
    //This method adds an informationSender into the database
    public boolean add(InformationSender informationSender){
        //Get the Database to operate on
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        //Use content values to add to the database
        ContentValues contentValues = new ContentValues();

        //Try to apply this variable
        try{
            //get the values and allign them to their table
            contentValues = fillContentValues(contentValues, informationSender);
            db.insert(databaseHelper.TABLE_INFORMATIONSENDER, null, contentValues);
            return true;
        }catch(Exception e){
            e.getMessage();
            return false;
        }


    }

    //update informationSender in the database
    public boolean update(InformationSender informationSender){
        //Get the Database to operate on
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        //Use content values to add to the database
        ContentValues contentValues = null;
        int id = informationSender.getInformationSenderId();
        String where = "informationSenderId=?";
        String[] whereArgs = new String[] {String.valueOf(id)};
        //Try to apply this variable
        try{
            //get the values and allign them to their table
            contentValues = fillContentValues(contentValues, informationSender);
            db.update(databaseHelper.TABLE_INFORMATIONSENDER, contentValues, where, whereArgs);
            return true;
        }catch(Exception e){
            e.getMessage();
            return false;
        }
    }




    //This method creates a InforamtionSender from a ResultSet.
    //This is helpful for generating large amounts of objects easilt
    public static InformationSender parseInformationSender(Cursor cur) throws SQLException {
        //Create a new InformationSender
        InformationSender informationSender = new InformationSender();

        //Define its variables
        informationSender.setInformationSenderId(cur.getInt(cur.getColumnIndex("informationSenderId")));
        informationSender.setClassId(cur.getInt(cur.getColumnIndex("classId")));
        informationSender.setInformation(cur.getString(cur.getColumnIndex("information")));
        informationSender.setTitle(cur.getString(cur.getColumnIndex("title")));
        //Return this InformationSender to the caller
        return informationSender;
    }

    //This is another helper method
    //This one helps fill a prepared statement
    public ContentValues fillContentValues(ContentValues contentValues, InformationSender informationSender) throws SQLException {
        contentValues.put(databaseHelper.CLASSID, informationSender.getClassId());
        contentValues.put(databaseHelper.TITLE, informationSender.getTitle());
        contentValues.put(databaseHelper.INFORMATION, informationSender.getInformation());
        return contentValues;
    }
}
