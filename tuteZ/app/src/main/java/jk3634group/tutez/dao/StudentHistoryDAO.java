package jk3634group.tutez.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import jk3634group.tutez.dto.StudentHistory;

/**
 * Created by homeworld on 10/21/16.
 */

//This class is a data access object for StudentHistory.
//The primary purpose for this class is to provide all the necessary database CRUD operations
//this improves ease of use in other code
// ---- this was also the quickest way to make large amounts of functionality's

//Must extend the BaseDAO for database accessing needs
public class StudentHistoryDAO extends BaseDAO {


    public StudentHistoryDAO(Context context) {
        super(context);
    }


    //Get every assignment basic select statements
    public ArrayList<StudentHistory> getAllstudentHistory(int id){
        //Create a returnable ArrayList
        //This will hold all the objest for all the assignments
        ArrayList<StudentHistory> studentHistories = new ArrayList<StudentHistory>();

        //Get the Database to operate on
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM studentHistory WHERE studentId=" +id, null);



        //Get an assignment from this cursor
        while (cur.moveToNext()) {

            //Attempt to assign assignment
            try {
                StudentHistory studentHistory = null;
                studentHistory = parseStudentHistory(cur);
                studentHistories.add(studentHistory);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return studentHistories;
    }

    //Determine if Student needs to be added or updated
    public void addOrUpdate(StudentHistory studentHistory)  {
        if (studentHistory.getStudentHistoryId() > 0) {
            update(studentHistory);
        } else {
            add(studentHistory);
        }
    }

    //Add a student record to the database
    //This method adds a student into the database
    public boolean add(StudentHistory studentHistory){
        //Get the Database to operate on
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        //Use content values to add to the database
        ContentValues contentValues = new ContentValues();

        //Try to apply this variable
        try{
            //get the values and allign them to their table
            contentValues = fillContentValues(contentValues, studentHistory);
            db.insert(databaseHelper.TABLE_STUDENTHISTORY, null, contentValues);
            return true;
        }catch(Exception e){
            e.getMessage();
            return false;
        }

    }

    //update student in the database
    public boolean update(StudentHistory studentHistory){
        //Get the Database to operate on
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        //Use content values to add to the database
        ContentValues contentValues = null;
        int id = studentHistory.getStudentHistoryId();
        String where = "studentHistoryId=?";
        String[] whereArgs = new String[] {String.valueOf(id)};
        //Try to apply this variable
        try{
            //get the values and allign them to their table
            contentValues = fillContentValues(contentValues, studentHistory);
            db.update(databaseHelper.TABLE_STUDENTHISTORY, contentValues, where, whereArgs);
            return true;
        }catch(Exception e){
            e.getMessage();
            return false;
        }
    }

    //Now we are going to delete based on student id
    //This helps the system clear all the presence a student had on a system.
    public boolean deleteByStudentId(int id){
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        try {
            db.execSQL("DELETE FROM " + databaseHelper.TABLE_STUDENTHISTORY + " WHERE studentId='" + id + "'");
            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }

    //Final DAO for deleting a student history object
    //Deletes a student history record
    public boolean delete(StudentHistory studentHistory){
        int studentHistoryId = studentHistory.getStudentHistoryId();
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        try {
            db.execSQL("DELETE FROM " + databaseHelper.TABLE_STUDENTHISTORY + " WHERE studentHistoryId='" + studentHistoryId + "'");
            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }



    //ID => studentId => category => description
    //This method creates a InforamtionSender from a ResultSet.
    //This is helpful for generating large amounts of objects easilt
    public static StudentHistory parseStudentHistory(Cursor cur) throws SQLException {
        //Create a new StudentHistory
        StudentHistory studentHistory = new StudentHistory();

        //Define its variables
        studentHistory.setStudentHistoryId(cur.getInt(cur.getColumnIndex("studentHistoryId")));
        studentHistory.setStudentId(cur.getInt(cur.getColumnIndex("studentId")));
        studentHistory.setCategory(cur.getString(cur.getColumnIndex("category")));
        studentHistory.setDescription(cur.getString(cur.getColumnIndex("description")));
        //Return this StudentHistory to the caller
        return studentHistory;
    }

    //This is another helper method
    //This one helps fill a prepared statement
    public ContentValues fillContentValues(ContentValues contentValues, StudentHistory studentHistory) throws SQLException {
        contentValues.put(databaseHelper.STUDENTID, studentHistory.getStudentId());
        contentValues.put(databaseHelper.CATEGORY, studentHistory.getCategory());
        contentValues.put(databaseHelper.DESCRIPTION, studentHistory.getDescription());
        return contentValues;
    }
}
