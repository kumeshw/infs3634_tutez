package jk3634group.tutez.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import jk3634group.tutez.dto.Results;

/**
 * Created by homeworld on 10/21/16.
 */

//This class is a data access object for Results.
//The primary purpose for this class is to provide all the necessary database CRUD operations
//this improves ease of use in other code
// ---- this was also the quickest way to make large amounts of functionality's

//Must extend the BaseDAO for database accessing needs
public class ResultsDAO extends BaseDAO {


    public ResultsDAO(Context context) {
        super(context);
    }


    //Simple select based on Student ID
    public ArrayList<Results> getResultsByStudentId(int id){

        //create an arraylist for all the results
        ArrayList<Results> results = new ArrayList<Results>();

        //Get the Database to operate on
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM results WHERE studentId = "+id, null);

        //Get an assignment from this cursor
        while (cur.moveToNext()) {

            //Attempt to assign assignment
            try {
                Results result = null;
                result = parseResults(cur);
                results.add(result);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return results;

    }

    //Simple select based on Student ID
    public ArrayList<Results> getResultsByAssignmentId(int id){

        //create an arraylist for all the results
        ArrayList<Results> results = new ArrayList<Results>();

        //Get the Database to operate on
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM results WHERE assignmentId = "+id, null);

        //Get an assignment from this cursor
        while (cur.moveToNext()) {

            //Attempt to assign assignment
            try {
                Results result = null;
                result = parseResults(cur);
                results.add(result);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return results;

    }

    //Determine if Student needs to be added or updated
    public void addOrUpdate(Results result)  {
        if (result.getResultId() > 0) {
            update(result);
        } else {
            add(result);
        }
    }

    //Add a student record to the database
    //This method adds a student into the database
    public boolean add(Results result){
        //Get the Database to operate on
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        //Use content values to add to the database
        ContentValues contentValues = new ContentValues();

        //Try to apply this variable
        try{
            //get the values and allign them to their table
            contentValues = fillContentValues(contentValues, result);
            db.insert(databaseHelper.TABLE_RESULTS, null, contentValues);
            return true;
        }catch(Exception e){
            e.getMessage();
            return false;
        }

    }

    //update student in the database
    public boolean update(Results result){
        //Get the Database to operate on
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        //Use content values to add to the database
        ContentValues contentValues = null;
        int id = result.getResultId();
        String where = "resultId=?";
        String[] whereArgs = new String[] {String.valueOf(id)};
        //Try to apply this variable
        try{
            //get the values and allign them to their table
            contentValues = fillContentValues(contentValues, result);
            db.update(databaseHelper.TABLE_RESULTS, contentValues, where, whereArgs);
            return true;
        }catch(Exception e){
            e.getMessage();
            return false;
        }
    }

    //Now we are going to delete based on Assignment id
    //This helps the system clear all the presence an assignment had on a system.
    public boolean deleteByAssignmentId(int id){
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        try {
            db.execSQL("DELETE FROM " + databaseHelper.TABLE_RESULTS + " WHERE assignmentId='" + id + "'");
            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }

    //Now we are going to delete based on student id
    //This helps the system clear all the presence a student had on a system.
    public boolean deleteByStudentId(int id){
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        try {
            db.execSQL("DELETE FROM " + databaseHelper.TABLE_RESULTS + " WHERE studentId='" + id + "'");
            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }

    //Final DAO for deleting a result object
    //Deletes a result record
    public boolean delete(Results result){
        int resultId = result.getResultId();
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        try {
            db.execSQL("DELETE FROM " + databaseHelper.TABLE_RESULTS + " WHERE resultId='" + resultId + "'");
            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }




    //ID => assignmentId => studentId => mark
    //This method creates a InforamtionSender from a ResultSet.
    //This is helpful for generating large amounts of objects easilt
    public static Results parseResults(Cursor cur) throws SQLException {
        //Create a new Results
        Results result = new Results();

        //Define its variables
        result.setResultId(cur.getInt(cur.getColumnIndex("resultId")));
        result.setAssignmentId(cur.getInt(cur.getColumnIndex("assignmentId")));
        result.setStudentId(cur.getInt(cur.getColumnIndex("studentId")));
        result.setMark(cur.getDouble(cur.getColumnIndex("mark")));

        //Return this Results to the caller
        return result;
    }

    //This is another helper method
    //This one helps fill a prepared statement
    public ContentValues fillContentValues(ContentValues contentValues, Results result) throws SQLException {
        contentValues.put(databaseHelper.ASSIGNMENTID, result.getAssignmentId());
        contentValues.put(databaseHelper.STUDENTID, result.getStudentId());
        contentValues.put(databaseHelper.MARK, result.getMark());
        return contentValues;
    }
}
