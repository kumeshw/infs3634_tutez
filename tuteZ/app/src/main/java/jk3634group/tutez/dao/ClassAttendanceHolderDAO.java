package jk3634group.tutez.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.sql.SQLException;
import java.util.ArrayList;

import jk3634group.tutez.dto.ClassAttendanceHolder;

/**
 * Created by homeworld on 10/21/16.
 */

//This class is a data access object for ClassAttendanceHolder.
//The primary purpose for this class is to provide all the necessary database CRUD operations
//this improves ease of use in other code
// ---- this was also the quickest way to make large amounts of functionality's

//Must extend the BaseDAO for database accessing needs
public class ClassAttendanceHolderDAO extends BaseDAO {


    public ClassAttendanceHolderDAO(Context context) {
        super(context);
    }


    public ArrayList<ClassAttendanceHolder> getAllClassAttendanceByClasssId(int id){
        //Create a returnable ArrayList
        //This will hold all the objest for all the assignments
        ArrayList<ClassAttendanceHolder> classAttendanceHolders = new ArrayList<ClassAttendanceHolder>();

        //Get the Database to operate on
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM classAttendenceHolder WHERE classId ="+id, null);



        //Get an assignment from this cursor
        while (cur.moveToNext()) {

            //Attempt to assign assignment
            try {
                ClassAttendanceHolder classAttendanceHolder = null;
                classAttendanceHolder = parseClassAttendanceHolder(cur);
                classAttendanceHolders.add(classAttendanceHolder);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return classAttendanceHolders;
    }

    public ClassAttendanceHolder getClassHolderByAttendanceRecord(String record){
        //Create a returnable ArrayList
        //This will hold all the objest for all the assignments
        //Get the Database to operate on
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM classAttendenceHolder WHERE attendanceRecord ='"+record+"'", null);

        ClassAttendanceHolder classAttendanceHolder = null;

        //Get an assignment from this cursor
        while (cur.moveToNext()) {

            //Attempt to assign assignment
            try {

                classAttendanceHolder = parseClassAttendanceHolder(cur);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return classAttendanceHolder;
    }

    //Determine if ClassAttendanceHolder needs to be added or updated
    public void addOrUpdate(ClassAttendanceHolder classAttendanceHolder)  {
        if (classAttendanceHolder.getClassAttendanceHolderId() > 0) {
            update(classAttendanceHolder);
        } else {
            add(classAttendanceHolder);
        }
    }

    //Add a class attendence record to the database
    //This method adds a class attendence into the database
    public boolean add(ClassAttendanceHolder classAttendanceHolder){
        //Get the Database to operate on
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        //Use content values to add to the database
        ContentValues contentValues = new ContentValues();

        //Try to apply this variable
        try{
            //get the values and allign them to their table
            contentValues = fillContentValues(contentValues, classAttendanceHolder);
            db.insert(databaseHelper.TABLE_CLASSATTENDENCEHOLDER, null, contentValues);
            return true;
        }catch(Exception e){
            e.getMessage();
            return false;
        }

    }


    //update student in the database
    public boolean update(ClassAttendanceHolder classAttendanceHolder){
        //Get the Database to operate on
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        //Use content values to add to the database
        ContentValues contentValues = null;
        int id = classAttendanceHolder.getClassAttendanceHolderId();
        String where = "classAttendanceHolderId=?";
        String[] whereArgs = new String[] {String.valueOf(id)};
        //Try to apply this variable
        try{
            //get the values and allign them to their table
            contentValues = fillContentValues(contentValues, classAttendanceHolder);
            db.update(databaseHelper.TABLE_CLASSATTENDENCEHOLDER, contentValues, where, whereArgs);
            return true;
        }catch(Exception e){
            e.getMessage();
            return false;
        }
    }

    //I cant see these being deleted by for now ill only delete based on class id for then sake of
    //removing a classes presence on a system
    public boolean deleteByClassAttendanceHolderId(int id){
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        try {
            db.execSQL("DELETE FROM " + databaseHelper.TABLE_CLASSATTENDENCEHOLDER + " WHERE classAttendanceHolderId='" + id + "'");
            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }

    //I cant see these being deleted by for now ill only delete based on class id for then sake of
    //removing a classes presence on a system
    public boolean deleteAll(){
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        try {
            db.execSQL("DELETE FROM " + databaseHelper.TABLE_CLASSATTENDENCEHOLDER);
            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }

    //ID => classId => studentId => didAttend
    //This method creates a InforamtionSender from a ResultSet.
    //This is helpful for generating large amounts of objects easily
    public static ClassAttendanceHolder parseClassAttendanceHolder(Cursor cur) throws SQLException {
        //Create a new Class
        ClassAttendanceHolder classAttendanceHolder = new ClassAttendanceHolder();

        //Define its variables
        classAttendanceHolder.setClassAttendanceHolderId(cur.getInt(cur.getColumnIndex("classAttendanceHolderId")));
        classAttendanceHolder.setAttendanceRecord(cur.getString(cur.getColumnIndex("attendanceRecord")));
        classAttendanceHolder.setClassId(cur.getInt(cur.getColumnIndex("classId")));

        //Return this Class to the caller
        return classAttendanceHolder;
    }

    //This is another helper method
    //This one helps fill a Content values
    public ContentValues fillContentValues(ContentValues contentValues, ClassAttendanceHolder classAttendanceHolder) throws SQLException {
        contentValues.put(databaseHelper.ATTENDANCERECORD, classAttendanceHolder.getAttendanceRecord());
        contentValues.put(databaseHelper.CLASSID, classAttendanceHolder.getClassId());
        return contentValues;
    }
}
