package jk3634group.tutez.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.sql.SQLException;
import java.util.ArrayList;

import jk3634group.tutez.dto.AssignmentClass;

/**
 * Created by homeworld on 10/21/16.
 */

//This class is a data access object for AssignmentClass.
//The primary purpose for this class is to provide all the necessary database CRUD operations
//this improves ease of use in other code
// ---- this was also the quickest way to make large amounts of functionality's

//Must extend the BaseDAO for database accessing needs
public class AssignmentClassDAO extends BaseDAO {


    public AssignmentClassDAO(Context context) {
        super(context);
    }

    //Get every assignment basic select statements
    public ArrayList<AssignmentClass> getAllAssignmentClassByClassId(int id){
        //Create a returnable ArrayList
        //This will hold all the objest for all the assignments
        ArrayList<AssignmentClass> assignmentClasses = new ArrayList<AssignmentClass>();

        //Get the Database to operate on
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM assignmentClass where classId="+id, null);



        //Get an assignment from this cursor
        while (cur.moveToNext()) {

            //Attempt to assign assignment
            try {
                AssignmentClass assignmentClass = null;
                assignmentClass = parseAssignmentClass(cur);
                assignmentClasses.add(assignmentClass);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return assignmentClasses;
    }

    //Get every assignment basic select statements
    public ArrayList<AssignmentClass> getAllAssignmentClassByAssignmentId(int id){
        //Create a returnable ArrayList
        //This will hold all the objest for all the assignments
        ArrayList<AssignmentClass> assignmentClasses = new ArrayList<AssignmentClass>();

        //Get the Database to operate on
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM assignmentClass WHERE assignmentId="+id, null);



        //Get an assignment from this cursor
        while (cur.moveToNext()) {

            //Attempt to assign assignment
            try {
                AssignmentClass assignmentClass = null;
                assignmentClass = parseAssignmentClass(cur);
                assignmentClasses.add(assignmentClass);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return assignmentClasses;
    }

    //Determine if AssignmentClass needs to be added or updated
    public void addOrUpdate(AssignmentClass assignmentClass)  {
        if (assignmentClass.getAssignmentClassId() > 0) {
            update(assignmentClass);
        } else {
            add(assignmentClass);
        }
    }

    //Add a AssignmentClass record to the database
    //This method adds a AssignmentClass into the database
    public boolean add(AssignmentClass assignmentClass){
        //Get the Database to operate on
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        //Use content values to add to the database
        ContentValues contentValues = new ContentValues();

        //Try to apply this variable
        try{
            //get the values and allign them to their table
            contentValues = fillContentValues(contentValues, assignmentClass);
            db.insert(databaseHelper.TABLE_ASSIGNMENTCLASS, null, contentValues);
            return true;
        }catch(Exception e){
            e.getMessage();
            return false;
        }

    }

    //update Assignment in the database
    public boolean update(AssignmentClass assignmentClass){
        //Get the Database to operate on
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        //Use content values to add to the database
        ContentValues contentValues = null;
        int id = assignmentClass.getAssignmentClassId();
        String where = "assignmentClassId=?";
        String[] whereArgs = new String[] {String.valueOf(id)};
        //Try to apply this variable
        try{
            //get the values and allign them to their table
            contentValues = fillContentValues(contentValues, assignmentClass);
            db.update(databaseHelper.TABLE_ASSIGNMENTCLASS, contentValues, where, whereArgs);
            return true;
        }catch(Exception e){
            e.getMessage();
            return false;
        }
    }

    //Now we are going to delete all
    public boolean deleteAll(){
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        try {
            db.execSQL("DELETE FROM " + databaseHelper.TABLE_ASSIGNMENTCLASS);
            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }

    //Now we are going to delete based on assignment id
    //This helps the system clear all the presence a assignment had on a system.
    public boolean deleteByAssignmentId(int id){
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        try {
            db.execSQL("DELETE FROM " + databaseHelper.TABLE_ASSIGNMENTCLASS + " WHERE assignmentId='" + id + "'");
            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }

    //Now we are going to delete based on class id
    //This helps the system clear all the presence a class had on a system.
    public boolean deleteByClassId(int id){
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        try {
            db.execSQL("DELETE FROM " + databaseHelper.TABLE_ASSIGNMENTCLASS + " WHERE classId='" + id + "'");
            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }



    //This method creates a InforamtionSender from a ResultSet.
    //This is helpful for generating large amounts of objects easilt
    public static AssignmentClass parseAssignmentClass(Cursor cur) throws SQLException {
        //Create a new AssignmentClass
        AssignmentClass assignmentClass = new AssignmentClass();

        //Define its variables
        assignmentClass.setAssignmentClassId(cur.getInt(cur.getColumnIndex("assigmentClassId")));
        assignmentClass.setAssignmentId(cur.getInt(cur.getColumnIndex("assignmentId")));
        assignmentClass.setClassId(cur.getInt(cur.getColumnIndex("classId")));
        //Return this AssignmentClass to the caller
        return assignmentClass;
    }

    //This is another helper method
    //This one helps fill a prepared statement
    public ContentValues fillContentValues(ContentValues contentValues, AssignmentClass assignmentClass) throws SQLException {
        contentValues.put(databaseHelper.ASSIGNMENTID, assignmentClass.getAssignmentId());
        contentValues.put(databaseHelper.CLASSID, assignmentClass.getClassId());
        return contentValues;
    }
}
