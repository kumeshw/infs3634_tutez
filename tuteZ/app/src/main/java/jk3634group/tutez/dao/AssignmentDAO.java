package jk3634group.tutez.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import java.sql.SQLException;
import java.util.ArrayList;

import jk3634group.tutez.dto.Assignments;

/**
 * Created by homeworld on 10/21/16.
 */

//This class is a data access object for Assignments.
//The primary purpose for this class is to provide all the necessary database CRUD operations
//this improves ease of use in other code
// ---- this was also the quickest way to make large amounts of functionality's

//Must extend the BaseDAO for database accessing needs
public class AssignmentDAO extends BaseDAO {

    //Fulfull superclass constructor needs
    //We should gain access to all our CRUD needs now
    public AssignmentDAO(Context context) {
        super(context);
    }

    //Simple select based on Assingment ID
    public Assignments getAssignmentById(int id){
        //Get the Database to operate on
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM assignments WHERE assignmentId = "+id, null);

        Assignments assignment = null;

        //Get an assignment from this cursor
        while (cur.moveToNext()) {

            //Attempt to assign assignment
            try {
                assignment = parseAssignments(cur);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return assignment;
    }

    //Simple select based on Assingment ID
    public Assignments getAssignmentByName(String name){
        //Get the Database to operate on
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM assignments WHERE assignmentName = '"+name+"'", null);

        Assignments assignment = null;

        //Get an assignment from this cursor
        while (cur.moveToNext()) {

            //Attempt to assign assignment
            try {
                assignment = parseAssignments(cur);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return assignment;
    }
    //Get every assignment basic select statements
    public ArrayList<Assignments> getAllAssignments(){
        //Create a returnable ArrayList
        //This will hold all the objest for all the assignments
        ArrayList<Assignments> assignments = new ArrayList<Assignments>();

        //Get the Database to operate on
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM assignments", null);



        //Get an assignment from this cursor
        while (cur.moveToNext()) {

            //Attempt to assign assignment
            try {
                Assignments assignment = null;
                assignment = parseAssignments(cur);
                assignments.add(assignment);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return assignments;
    }

    //Determine if Assignment needs to be added or updated
    public void addOrUpdate(Assignments assignment)  {
        if (assignment.getAssignmentId() > 0) {
            update(assignment);
        } else {
            add(assignment);
        }
    }

    //Add an assignmnet record to the database
    //This method adds an assignment into the database
    public boolean add(Assignments assignment){
        //Get the Database to operate on
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        //Use content values to add to the database
        ContentValues contentValues = new ContentValues();

        //Try to apply this variable
        try{
            //get the values and allign them to their table
            contentValues = fillContentValues(contentValues, assignment);
            db.insert(databaseHelper.TABLE_ASSIGNMENTS, null, contentValues);
            return true;
        }catch(Exception e){
            e.getMessage();
            return false;
        }


    }

    //update assignment in the database
    public boolean update(Assignments assignment){
        //Get the Database to operate on
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        //Use content values to add to the database
        ContentValues contentValues = null;
        int id = assignment.getAssignmentId();
        String where = "assignmentId=?";
        String[] whereArgs = new String[] {String.valueOf(id)};
        //Try to apply this variable
        try{
            //get the values and allign them to their table
            contentValues = fillContentValues(contentValues, assignment);
            db.update(databaseHelper.TABLE_ASSIGNMENTS, contentValues, where, whereArgs);
            return true;
        }catch(Exception e){
            e.getMessage();
            return false;
        }
    }

    //Now we are going to delete based on assignment
    public boolean delete(Assignments assignment){
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        int assignmentId = assignment.getAssignmentId();
        try {
            db.execSQL("DELETE FROM " + databaseHelper.TABLE_ASSIGNMENTS + " WHERE assignmentId='" + assignmentId + "'");
            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }

    //Now we are going to delete based all
    public boolean deleteAll(){
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        try {
            db.execSQL("DELETE FROM " + databaseHelper.TABLE_ASSIGNMENTS );
            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }


    //AssignmentID => AssignmentClassId => AssignmentDescription => AssignmentName
    //This method creates a InforamtionSender from a ResultSet.
    //This is helpful for generating large amounts of objects easilt
    public static Assignments parseAssignments(Cursor cur) throws SQLException {
        //Create a new Assignments
        Assignments assignment = new Assignments();
        assignment.setAssignmentId(cur.getInt(cur.getColumnIndex("assignmentId")));
        assignment.setAssignmentDescription(cur.getString(cur.getColumnIndex("assignmentDescription")));
        assignment.setAssignmentName(cur.getString(cur.getColumnIndex("assignmentName")));

        //Return this Assignments to the caller
        return assignment;
    }

    //This is another helper method
    //This one helps fill ContentValues
    public ContentValues fillContentValues(ContentValues contentValues, Assignments assignment) throws SQLException {
        contentValues.put(databaseHelper.ASSIGNMENTNAME, assignment.getAssignmentName());
        contentValues.put(databaseHelper.ASSIGNMENTDESCRIPTION, assignment.getAssignmentDescription());
        return contentValues;
    }
}
