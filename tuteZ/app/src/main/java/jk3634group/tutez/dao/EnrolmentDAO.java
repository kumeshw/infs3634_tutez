package jk3634group.tutez.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import jk3634group.tutez.dto.Enrolment;

/**
 * Created by homeworld on 10/21/16.
 */

//This class is a data access object for Enrolment.
//The primary purpose for this class is to provide all the necessary database CRUD operations
//this improves ease of use in other code
// ---- this was also the quickest way to make large amounts of functionality's

//Must extend the BaseDAO for database accessing needs
public class EnrolmentDAO extends BaseDAO {


    public EnrolmentDAO(Context context) {
        super(context);
    }

    //Get every assignment basic select statements
    //this method basically finds all the student enrolments by one class
    public ArrayList<Enrolment> getAllEnrolmentsByClassId(int id){
        //Create a returnable ArrayList
        //This will hold all the objest for all the assignments
        ArrayList<Enrolment> enrolments = new ArrayList<Enrolment>();

        //Get the Database to operate on
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM enrolment WHERE classId="+id, null);



        //Get an assignment from this cursor
        while (cur.moveToNext()) {

            //Attempt to assign assignment
            try {
                Enrolment enrolment = null;
                enrolment = parseEnrolment(cur);
                enrolments.add(enrolment);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return enrolments;
    }

    //Get every assignment basic select statements
    //this finds all the student enrolments by student
    public ArrayList<Enrolment> getAllEnrolmentsByStudentId(int id){
        //Create a returnable ArrayList
        //This will hold all the objest for all the assignments
        ArrayList<Enrolment> enrolments = new ArrayList<Enrolment>();

        //Get the Database to operate on
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM enrolment WHERE studentId="+id, null);



        //Get an assignment from this cursor
        while (cur.moveToNext()) {

            //Attempt to assign assignment
            try {
                Enrolment enrolment = null;
                enrolment = parseEnrolment(cur);
                enrolments.add(enrolment);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return enrolments;
    }
    //Determine if Enrolment needs to be added or updated
    public void addOrUpdate(Enrolment enrolment)  {
        if (enrolment.getEnrolmentId() > 0) {
            update(enrolment);
        } else {
            add(enrolment);
        }
    }

    //Add a enrolment record to the database
    //This method adds a enrolment into the database
    public boolean add(Enrolment enrolment){
        //Get the Database to operate on
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        //Use content values to add to the database
        ContentValues contentValues = new ContentValues();

        //Try to apply this variable
        try{
            //get the values and allign them to their table
            contentValues = fillContentValues(contentValues, enrolment);
            db.insert(databaseHelper.TABLE_ENROLMENT, null, contentValues);
            return true;
        }catch(Exception e){
            e.getMessage();
            return false;
        }

    }

    //update enrolment in the database
    public boolean update(Enrolment enrolment){
        //Get the Database to operate on
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        //Use content values to add to the database
        ContentValues contentValues = null;
        int id = enrolment.getEnrolmentId();
        String where = "enrolmentId=?";
        String[] whereArgs = new String[] {String.valueOf(id)};
        //Try to apply this variable
        try{
            //get the values and allign them to their table
            contentValues = fillContentValues(contentValues, enrolment);
            db.update(databaseHelper.TABLE_ENROLMENT, contentValues, where, whereArgs);
            return true;
        }catch(Exception e){
            e.getMessage();
            return false;
        }
    }


    //Now we are going to delete based on student id
    //This helps the system clear all the presence a student had on a system.
    public boolean deleteByStudentId(int id){
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        try {
            db.execSQL("DELETE FROM " + databaseHelper.TABLE_ENROLMENT + " WHERE studentId='" + id + "'");
            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }

    //Now we are going to delete based on student id
    //This helps the system clear all the presence a student had on a system.
    public boolean deleteAll(){
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        try {
            db.execSQL("DELETE FROM " + databaseHelper.TABLE_ENROLMENT );
            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }

    //Now we are going to delete based on class id
    //This helps the system clear all the presence a class had on a system.
    public boolean deleteByClassId(int id){
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        try {
            db.execSQL("DELETE FROM " + databaseHelper.TABLE_ENROLMENT + " WHERE classId='" + id + "'");
            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }

    //Method for deleting an Enrolment object
    public boolean delete(Enrolment enrolment){
        int enrolmentId = enrolment.getEnrolmentId();
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        try {
            db.execSQL("DELETE FROM " + databaseHelper.TABLE_ENROLMENT + " WHERE enrolmentId='" + enrolmentId + "'");
            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }


    //EnrolID => enrolmentId => classId
    //This method creates a InforamtionSender from a ResultSet.
    //This is helpful for generating large amounts of objects easilt
    public static Enrolment parseEnrolment(Cursor cur) throws SQLException {
        //Create a new Enrolment
        Enrolment enrolment = new Enrolment();

        //Define its variables
        enrolment.setEnrolmentId(cur.getInt(cur.getColumnIndex("enrolmentId")));
        enrolment.setStudentId(cur.getInt(cur.getColumnIndex("studentId")));
        enrolment.setClassId(cur.getInt(cur.getColumnIndex("classId")));
        //Return this Enrolment to the caller
        return enrolment;
    }

    //This is another helper method
    //This one helps fill a ContentValues
    public ContentValues fillContentValues(ContentValues contentValues, Enrolment enrolment) throws SQLException {
        contentValues.put(databaseHelper.STUDENTID, enrolment.getStudentId());
        contentValues.put(databaseHelper.CLASSID, enrolment.getClassId());
        return contentValues;
    }
}
