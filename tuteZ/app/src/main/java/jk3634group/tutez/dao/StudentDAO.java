package jk3634group.tutez.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import java.sql.SQLException;
import java.util.ArrayList;
import jk3634group.tutez.dto.Student;

/**
 * Created by homeworld on 10/21/16.
 */

//This class is a data access object for Students.
//The primary purpose for this class is to provide all the necessary database CRUD operations
//this improves ease of use in other code
// ---- this was also the quickest way to make large amounts of functionality's

//Must extend the BaseDAO for database accessing needs
public class StudentDAO extends BaseDAO {


    public StudentDAO(Context context) {
        super(context);
    }

    //Simple select based on Student ID
    public Student getStudentsById(int id){
        //Get the Database to operate on
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM student WHERE studentId = "+id, null);

        Student student = null;

        //Get an assignment from this cursor
        while (cur.moveToNext()) {

            //Attempt to assign assignment
            try {
                student = parseStudent(cur);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return student;
    }

    //Get every assignment basic select statements
    public ArrayList<Student> getAllstudents(){
        //Create a returnable ArrayList
        //This will hold all the objest for all the assignments
        ArrayList<Student> students = new ArrayList<Student>();

        //Get the Database to operate on
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM student", null);



        //Get an assignment from this cursor
        while (cur.moveToNext()) {

            //Attempt to assign assignment
            try {
                Student student = null;
                student = parseStudent(cur);
                students.add(student);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return students;
    }

    //Determine if Student needs to be added or updated
    public void addOrUpdate(Student student)  {
        if (student.getStudentId() > 0) {
            update(student);
        } else {
            add(student);
        }
    }

    //Add a student record to the database
    //This method adds a student into the database
    public boolean add(Student student){
        //Get the Database to operate on
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        //Use content values to add to the database
        ContentValues contentValues = new ContentValues();

        //Try to apply this variable
        try{
            //get the values and align them to their table
            contentValues = fillContentValues(contentValues, student);
            db.insert(databaseHelper.TABLE_STUDENT, null, contentValues);
            return true;
        }catch(Exception e){
            e.getMessage();
            return false;
        }

    }

    //update student in the database
    public boolean update(Student student){
        //Get the Database to operate on
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        //Use content values to add to the database
        ContentValues contentValues = null;
        int id = student.getStudentId();
        String where = "studentId=?";
        String[] whereArgs = new String[] {String.valueOf(id)};
        //Try to apply this variable
        try{
            //get the values and allign them to their table
            contentValues = fillContentValues(contentValues, student);
            db.update(databaseHelper.TABLE_STUDENT, contentValues, where, whereArgs);
            return true;
        }catch(Exception e){
            e.getMessage();
            return false;
        }
    }




    //This method creates a student from a ResultSet.
    //This is helpful for generating large amou//Final DAO for deleting a student object
    //Deletes a student record
    public boolean delete(Student student){
        int studentId = student.getStudentId();
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        try {
            db.execSQL("DELETE FROM " + databaseHelper.TABLE_STUDENT + " WHERE studentId='" + studentId + "'");

            //Since we are deleting a student, all of this students presences has to be purged from the system as well
            //Basically, we have to remove this students Results and enrolments and student history as well
            //This is handled in the method that deletes a student

            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }

    //This method creates a student from a ResultSet.
    //This is helpful for generating large amou//Final DAO for deleting a student object
    //Deletes a student record
    public boolean deleteAll(){
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        try {
            db.execSQL("DELETE FROM " + databaseHelper.TABLE_STUDENT );

            //Since we are deleting a student, all of this students presences has to be purged from the system as well
            //Basically, we have to remove this students Results and enrolments and student history as well
            //This is handled in the method that deletes a student

            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }

    //Creates a student object from cursor
    //Helps minimise code
    public static Student parseStudent(Cursor cur) throws SQLException {
        //Create a new Student
        Student student = new Student();

        //Define its variables
        student.setStudentId(cur.getInt(cur.getColumnIndex("studentId")));
        student.setFirstName(cur.getString(cur.getColumnIndex("firstName")));
        student.setLastName(cur.getString(cur.getColumnIndex("lastName")));
        student.setEmail(cur.getString(cur.getColumnIndex("email")));

        //Return this student to the caller
        return student;
    }

    //This is another helper method
    //This one helps fill a prepared statement
    public ContentValues fillContentValues(ContentValues contentValues, Student student) throws SQLException {
        contentValues.put(databaseHelper.FIRSTNAME, student.getFirstName());
        contentValues.put(databaseHelper.LASTNAME, student.getLastName());
        contentValues.put(databaseHelper.EMAIL, student.getEmail());
        return contentValues;
    }
}
