package jk3634group.tutez.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import java.sql.SQLException;
import java.util.ArrayList;
import jk3634group.tutez.dto.Class;

/**
 * Created by homeworld on 10/21/16.
 */

//This class is a data access object for Classs.
//The primary purpose for this class is to provide all the necessary database CRUD operations
//this improves ease of use in other code
// ---- this was also the quickest way to make large amounts of functionality's

//Must extend the BaseDAO for database accessing needs
public class ClassDAO extends BaseDAO {


    public ClassDAO(Context context) {
        super(context);
    }

    //Simple select based on Class ID
    public Class getClasssById(int id){
        //Get the Database to operate on
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM class WHERE classId = "+id, null);

        Class classObj = null;

        //Get an assignment from this cursor
        while (cur.moveToNext()) {

            //Attempt to assign assignment
            try {
                classObj = parseClass(cur);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return classObj;
    }

    //Simple select based on Class Cpde
    public Class getClasssByClassCode(String code){
        //Get the Database to operate on
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM class WHERE classCode = '"+code+"'", null);

        Class classObj = null;

        //Get an assignment from this cursor
        while (cur.moveToNext()) {

            //Attempt to assign assignment
            try {
                classObj = parseClass(cur);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return classObj;
    }

    //Get every assignment basic select statements
    public ArrayList<Class> getAllClasses(){
        //Create a returnable ArrayList
        //This will hold all the objest for all the assignments
        ArrayList<Class> classObjs = new ArrayList<Class>();

        //Get the Database to operate on
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        Cursor cur = db.rawQuery("SELECT * FROM class", null);



        //Get an assignment from this cursor
        while (cur.moveToNext()) {

            //Attempt to assign assignment
            try {
                Class classObj = null;
                classObj = parseClass(cur);
                classObjs.add(classObj);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return classObjs;
    }



    //Determine if Class needs to be added or updated
    public void addOrUpdate(Class classObj)  {
        if (classObj.getClassID() > 0) {
            update(classObj);
        } else {
            add(classObj);
        }
    }

    //Add a classObj record to the database
    //This method adds a classObj into the database
    public boolean add(Class classObj){
        //Get the Database to operate on
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        //Use content values to add to the database
        ContentValues contentValues = new ContentValues();

        //Try to apply this variable
        try{
            //get the values and allign them to their table
            contentValues = fillContentValues(contentValues, classObj);
            db.insert(databaseHelper.TABLE_CLASS, null, contentValues);
            return true;
        }catch(Exception e){
            e.getMessage();
            return false;
        }

    }

    //update classObj in the database
    public boolean update(Class classObj){
        //Get the Database to operate on
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        //Use content values to add to the database
        ContentValues contentValues = null;
        int id = classObj.getClassID();
        String where = "classId=?";
        String[] whereArgs = new String[] {String.valueOf(id)};
        //Try to apply this variable
        try{
            //get the values and allign them to their table
            contentValues = fillContentValues(contentValues, classObj);
            db.update(databaseHelper.TABLE_CLASS, contentValues, where, whereArgs);
            return true;
        }catch(Exception e){
            e.getMessage();
            return false;
        }
    }

    //I cant see these being deleted by for now ill only delete based on class id for then sake of
    //removing a classes presence on a system
    public boolean delete(){
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        try {
            db.execSQL("DELETE FROM " + databaseHelper.TABLE_CLASS);
            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }

    //I cant see these being deleted by for now ill only delete based on class id for then sake of
    //removing a classes presence on a system
    public boolean deleteById(int id){
        SQLiteDatabase db = databaseHelper.getWritableDatabase();
        try {
            db.execSQL("DELETE FROM " + databaseHelper.TABLE_CLASS + " WHERE classId =" +id);
            return true;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }



    //This method creates a classObj from a ResultSet.
    //This is helpful for generating large amounts of objects easilt
    public static Class parseClass(Cursor cur) throws SQLException {
        //Create a new Class
        Class classObj = new Class();

        //Define its variables
        classObj.setClassID(cur.getInt(cur.getColumnIndex("classId")));
        classObj.setClassName(cur.getString(cur.getColumnIndex("className")));
        classObj.setClassCode(cur.getString(cur.getColumnIndex("classCode")));
        classObj.setYear(cur.getInt(cur.getColumnIndex("year")));
        classObj.setSemester(cur.getInt(cur.getColumnIndex("semester")));
        //Return this classObj to the caller
        return classObj;
    }

    //This is another helper method
    //This one helps fill a prepared statement
    public ContentValues fillContentValues(ContentValues contentValues, Class classObj) throws SQLException {
        contentValues.put(databaseHelper.CLASSNAME, classObj.getClassName());
        contentValues.put(databaseHelper.CLASSCODE, classObj.getClassCode());
        contentValues.put(databaseHelper.YEAR, classObj.getYear());
        contentValues.put(databaseHelper.SEMESTER, classObj.getSemester());
        return contentValues;
    }
}
