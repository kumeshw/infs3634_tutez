package jk3634group.tutez.dao;

/**
 * Created by homeworld on 10/21/16.
 */

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import java.sql.SQLException;

import jk3634group.tutez.databasemanager.DatabaseHelper;


//This class will be the basis of the data CRUD operations.
//From this class, every other DAO will be able to access their data in the database.

public class BaseDAO{

    //Define private variables
    protected SQLiteDatabase database;
    protected DatabaseHelper databaseHelper;
    private Context mContext;

    //Create the constructor
    public BaseDAO(Context context){
        this.mContext = context;
        databaseHelper = DatabaseHelper.getHelper(mContext);
        try {
            open();
        } catch (SQLException e) {
            //I dont know if this should be a log or not?
            e.printStackTrace();
        }
    }

    //Open the database helper
    public void open() throws SQLException {
        if(databaseHelper == null)
            databaseHelper = DatabaseHelper.getHelper(mContext);
        database = databaseHelper.getWritableDatabase();
    }



}

