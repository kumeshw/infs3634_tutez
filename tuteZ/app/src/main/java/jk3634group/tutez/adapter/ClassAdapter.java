package jk3634group.tutez.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import jk3634group.tutez.R;
import jk3634group.tutez.activity.ClassProfile;
import jk3634group.tutez.dao.ClassDAO;
import jk3634group.tutez.dto.Class;


/**
 * Created by Dooba on 8/30/2016.
 */

public class ClassAdapter extends RecyclerView.Adapter<ClassAdapter.ClassViewHolder> implements Filterable {

    private Context context;
    public ArrayList<Class> classObjs, filterList;
    private CustomFilterClass filter;


    public ClassAdapter(Context context, ArrayList<Class> classObjs) {
        this.context = context;
        this.classObjs = classObjs;
        this.filterList = classObjs;
    }

    View view;

    @Override
    public ClassAdapter.ClassViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        view = inflater.from(parent.getContext()).inflate(R.layout.class_list_item, parent, false);

        ClassViewHolder classObjViewHolder = new ClassViewHolder(view);
        return classObjViewHolder;
    }

    @Override
    public void onBindViewHolder(ClassAdapter.ClassViewHolder holder, int position) {

        final Class currentClass = classObjs.get(position);
        holder.classObjName.setText(currentClass.getClassName());
        holder.classObjCode.setText(currentClass.getClassCode());

        ClassDAO classObjDAO = new ClassDAO(context);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Create an intent to go to this specific assigments profile
                Intent in = new Intent(context, ClassProfile.class);

                //Pass the assignment as an intent extra
                in.putExtra("class", currentClass);
                in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(in);

                Toast.makeText(context, currentClass.getClassName(), Toast.LENGTH_LONG).show();

            }
        });

        //implement click listener
        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                Snackbar.make(v, classObjs.get(position).getClassName(), Snackbar.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public int getItemCount() {
        if (classObjs != null) {
            return classObjs.size();
        } else {
            return 0;
        }
    }

    @Override
    public Filter getFilter() {

        if (filter == null) {
            filter = new CustomFilterClass(filterList, this);
        }

        return filter;
    }


    //view holder class
    public static class ClassViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView ivImgClass;
        public TextView classObjName;
        public TextView classObjCode;

        ItemClickListener itemClickListener;

        public ClassViewHolder(View itemView) {
            super(itemView);
            ivImgClass = (ImageView) itemView.findViewById(R.id.ivImgClass);
            classObjName = (TextView) itemView.findViewById(R.id.className);
            classObjCode = (TextView) itemView.findViewById(R.id.classCode);
        }

        @Override
        public void onClick(View v) {
            this.itemClickListener.onItemClick(v, getLayoutPosition());
        }

        public void setItemClickListener(ItemClickListener ic) {
            this.itemClickListener = ic;
        }
    }


}
