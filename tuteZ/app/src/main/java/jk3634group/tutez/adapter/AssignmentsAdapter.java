package jk3634group.tutez.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import jk3634group.tutez.R;
import jk3634group.tutez.activity.AssignmentProfile;
import jk3634group.tutez.dao.AssignmentDAO;
import jk3634group.tutez.dto.Assignments;


/**
 * Created by Dooba on 8/30/2016.
 */

public class AssignmentsAdapter extends RecyclerView.Adapter<AssignmentsAdapter.AssignmentsViewHolder> implements Filterable {

    private Context context;
    public ArrayList<Assignments> assignments, filterList;
    private CustomFilterAssignments filter;


    public AssignmentsAdapter(Context context, ArrayList<Assignments> assignments) {
        this.context = context;
        this.assignments = assignments;
        this.filterList = assignments;
    }

    View view;

    @Override
    public AssignmentsAdapter.AssignmentsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        view = inflater.from(parent.getContext()).inflate(R.layout.assignment_list_item, parent, false);

        AssignmentsViewHolder assignmentsViewHolder = new AssignmentsViewHolder(view);
        return assignmentsViewHolder;
    }

    @Override
    public void onBindViewHolder(AssignmentsAdapter.AssignmentsViewHolder holder, int position) {

        final Assignments currentAssignments = assignments.get(position);
        holder.assignmentsName.setText(currentAssignments.getAssignmentName());
        holder.assignmentsInfo.setText(currentAssignments.getAssignmentDescription());

        //Check for clicks

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Create an intent to go to this specific assigments profile
                Intent in = new Intent(context, AssignmentProfile.class);

                //Pass the assignment as an intent extra
                in.putExtra("assignment", currentAssignments);
                in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(in);

                Toast.makeText(context, currentAssignments.getAssignmentName(), Toast.LENGTH_LONG).show();

            }
        });


    }


    @Override
    public int getItemCount() {
        if (assignments != null) {
            return assignments.size();
        } else {
            return 0;
        }
    }

    @Override
    public Filter getFilter() {

        if (filter == null) {
            filter = new CustomFilterAssignments(filterList, this);
        }

        return filter;
    }


    //view holder class
    public static class AssignmentsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView ivImgAssignments;
        public TextView assignmentsName;
        public TextView assignmentsInfo;

        ItemClickListener itemClickListener;

        public AssignmentsViewHolder(View itemView) {
            super(itemView);
            ivImgAssignments = (ImageView) itemView.findViewById(R.id.ivImgAssignment);
            assignmentsName = (TextView) itemView.findViewById(R.id.assignmentName);
            assignmentsInfo = (TextView) itemView.findViewById(R.id.assignmentInfo);
        }

        @Override
        public void onClick(View v) {
            this.itemClickListener.onItemClick(v, getLayoutPosition());
        }

        public void setItemClickListener(ItemClickListener ic) {
            this.itemClickListener = ic;
        }
    }


}
