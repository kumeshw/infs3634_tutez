package jk3634group.tutez.adapter;

import android.widget.Filter;

import java.util.ArrayList;

import jk3634group.tutez.dto.Class;

/**
 * Created by Dooba on 10/1/2016.
 */
public class CustomFilterClass extends Filter {

    ClassAdapter adapter;
    ArrayList<Class> filterList;

    public CustomFilterClass(ArrayList<Class> filterList, ClassAdapter adapter){
        this.adapter = adapter;
        this.filterList = filterList;
    }

    //Now to filer results
    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results = new FilterResults();
        //check validity

        if(constraint != null && constraint.length() > 0){
            //change to uppercase
            constraint = constraint.toString().toUpperCase();


            //store filteredstudents
            ArrayList<Class> filteredStudents = new ArrayList<>();

            for(int i = 0; i < filterList.size(); i++){
                //check
                if(filterList.get(i).getClassCode().toUpperCase().contains(constraint) || filterList.get(i).getClassName().toUpperCase().contains(constraint)){
                    filteredStudents.add(filterList.get(i));
                }
            }
            results.count=filteredStudents.size();
            results.values=filteredStudents;
        } else {
            results.count=filterList.size();
            results.values=filterList;
        }

        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        adapter.classObjs=  (ArrayList<Class>) results.values;
        //refresh
        adapter.notifyDataSetChanged();

    }
}
