package jk3634group.tutez.adapter;

import android.widget.Filter;

import java.util.ArrayList;

import jk3634group.tutez.dto.ClassAttendanceHolder;

/**
 * Created by Dooba on 10/1/2016.
 */
public class CustomFilterClassAttendanceHolder extends Filter {

    ClassAttendanceAdapter adapter;
    ArrayList<ClassAttendanceHolder> filterList;

    public CustomFilterClassAttendanceHolder(ArrayList<ClassAttendanceHolder> filterList, ClassAttendanceAdapter adapter){
        this.adapter = adapter;
        this.filterList = filterList;
    }

    //Now to filer results
    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results = new FilterResults();
        //check validity

        if(constraint != null && constraint.length() > 0){
            //change to uppercase
            constraint = constraint.toString().toUpperCase();


            //store filteredstudents
            ArrayList<ClassAttendanceHolder> filteredClasses = new ArrayList<>();

            for(int i = 0; i < filterList.size(); i++){
                //check
                if(filterList.get(i).getAttendanceRecord().toUpperCase().contains(constraint)){
                    filteredClasses.add(filterList.get(i));
                }
            }
            results.count=filteredClasses.size();
            results.values=filteredClasses;
        } else {
            results.count=filterList.size();
            results.values=filterList;
        }

        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        adapter.classAttendanceHolders = (ArrayList<ClassAttendanceHolder>) results.values;
        //refresh
        adapter.notifyDataSetChanged();

    }
}
