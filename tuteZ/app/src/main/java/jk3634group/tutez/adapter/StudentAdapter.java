package jk3634group.tutez.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import java.util.ArrayList;

import jk3634group.tutez.R;
import jk3634group.tutez.activity.StudentProfile;
import jk3634group.tutez.dao.StudentDAO;
import jk3634group.tutez.dto.Student;


/**
 * Created by Dooba on 8/30/2016.
 */

public class StudentAdapter extends RecyclerView.Adapter<StudentAdapter.StudentViewHolder> implements Filterable {

    private Context context;
    public ArrayList<Student> students, filterList;
    private CustomFilter filter;


    public StudentAdapter(Context context, ArrayList<Student> students) {
        this.context = context;
        this.students = students;
        this.filterList = students;
    }

    View view;

    @Override
    public StudentAdapter.StudentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        view = inflater.from(parent.getContext()).inflate(R.layout.student_list_item, parent, false);

        StudentViewHolder studentViewHolder = new StudentViewHolder(view);
        return studentViewHolder;
    }

    @Override
    public void onBindViewHolder(StudentAdapter.StudentViewHolder holder, int position) {

        final Student currentStudent = students.get(position);
        holder.tvName.setText(currentStudent.getFirstName() + " " + currentStudent.getLastName());
        holder.tvEmail.setText(currentStudent.getEmail());

        StudentDAO studentDAO = new StudentDAO(context);

        final ArrayList<Student> students = studentDAO.getAllstudents();
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Create an intent to go to this specific students profile
                Intent in = new Intent(context, StudentProfile.class);

                //Pass the assignment as an intent extra
                in.putExtra("student", currentStudent);
                in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(in);

                Toast.makeText(context, currentStudent.getFirstName(), Toast.LENGTH_LONG).show();

            }
        });

        //implement click listener
        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                Snackbar.make(v, students.get(position).getFirstName(), Snackbar.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public int getItemCount() {
        if (students != null) {
            return students.size();
        } else {
            return 0;
        }
    }

    @Override
    public Filter getFilter() {

        if (filter == null) {
            filter = new CustomFilter(filterList, this);
        }

        return filter;
    }


    //view holder class
    public static class StudentViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView ivImg;
        public TextView tvName;
        public TextView tvEmail;

        ItemClickListener itemClickListener;

        public StudentViewHolder(View itemView) {
            super(itemView);
            ivImg = (ImageView) itemView.findViewById(R.id.ivImg);
            tvName = (TextView) itemView.findViewById(R.id.studentName);
            tvEmail = (TextView) itemView.findViewById(R.id.studentEmail);
        }

        @Override
        public void onClick(View v) {
            this.itemClickListener.onItemClick(v, getLayoutPosition());
        }

        public void setItemClickListener(ItemClickListener ic) {
            this.itemClickListener = ic;
        }
    }


}
