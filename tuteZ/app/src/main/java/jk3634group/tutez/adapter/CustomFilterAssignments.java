package jk3634group.tutez.adapter;

import android.widget.Filter;

import java.util.ArrayList;

import jk3634group.tutez.dto.Assignments;

/**
 * Created by Dooba on 10/1/2016.
 */
public class CustomFilterAssignments extends Filter {

    AssignmentsAdapter adapter;
    ArrayList<Assignments> filterList;

    public CustomFilterAssignments(ArrayList<Assignments> filterList, AssignmentsAdapter adapter){
        this.adapter = adapter;
        this.filterList = filterList;
    }

    //Now to filer results
    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results = new FilterResults();
        //check validity

        if(constraint != null && constraint.length() > 0){
            //change to uppercase
            constraint = constraint.toString().toUpperCase();


            //store filteredstudents
            ArrayList<Assignments> filteredStudents = new ArrayList<>();

            for(int i = 0; i < filterList.size(); i++){
                //check
                if(filterList.get(i).getAssignmentName().toUpperCase().contains(constraint)){
                    filteredStudents.add(filterList.get(i));
                }
            }
            results.count=filteredStudents.size();
            results.values=filteredStudents;
        } else {
            results.count=filterList.size();
            results.values=filterList;
        }

        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        adapter.assignments = (ArrayList<Assignments>) results.values;
        //refresh
        adapter.notifyDataSetChanged();

    }
}
