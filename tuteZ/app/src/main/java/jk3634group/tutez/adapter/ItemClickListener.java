package jk3634group.tutez.adapter;

import android.view.View;

/**
 * Created by Dooba on 10/1/2016.
 */
public interface ItemClickListener {
    void onItemClick(View v, int position);
}
