package jk3634group.tutez.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import jk3634group.tutez.R;
import jk3634group.tutez.activity.AttendanceForm;
import jk3634group.tutez.activity.ClassAttendanceHolderActivity;
import jk3634group.tutez.dto.ClassAttendanceHolder;


/**
 * Created by Dooba on 8/30/2016.
 */

public class ClassAttendanceAdapter extends RecyclerView.Adapter<ClassAttendanceAdapter.ClassAttendanceHolderViewHolder> implements Filterable {

    private Context context;
    public ArrayList<ClassAttendanceHolder> classAttendanceHolders, filterList;
    private CustomFilterClassAttendanceHolder filter;


    public ClassAttendanceAdapter(Context context, ArrayList<ClassAttendanceHolder> classAttendanceHolders) {
        this.context = context;
        this.classAttendanceHolders = classAttendanceHolders;
        this.filterList = classAttendanceHolders;
    }

    View view;

    @Override
    public ClassAttendanceAdapter.ClassAttendanceHolderViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        view = inflater.from(parent.getContext()).inflate(R.layout.attendance_list_item, parent, false);

        ClassAttendanceHolderViewHolder classAttendanceHoldersViewHolder = new ClassAttendanceHolderViewHolder(view);
        return classAttendanceHoldersViewHolder;
    }

    @Override
    public void onBindViewHolder(ClassAttendanceAdapter.ClassAttendanceHolderViewHolder holder, int position) {

        final ClassAttendanceHolder currentClassAttendanceHolder = classAttendanceHolders.get(position);
        holder.classAttendanceHoldersName.setText(currentClassAttendanceHolder.getAttendanceRecord());
        holder.classAttendanceHoldersInfo.setText("Class Attendance object");

        //Check for clicks


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Create an intent to go to this specific attendance form
                ((AppCompatActivity)context).finish();
                Intent in = new Intent(context, AttendanceForm.class);

                //Pass the assignment as an intent extra
                in.putExtra("attendance", currentClassAttendanceHolder);
                in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(in);

                Toast.makeText(context, currentClassAttendanceHolder.getAttendanceRecord(), Toast.LENGTH_LONG).show();

            }
        });



    }


    @Override
    public int getItemCount() {
        if (classAttendanceHolders != null) {
            return classAttendanceHolders.size();
        } else {
            return 0;
        }
    }

    @Override
    public Filter getFilter() {

        if (filter == null) {
            filter = new CustomFilterClassAttendanceHolder(filterList, this);
        }

        return filter;
    }


    //view holder class
    public static class ClassAttendanceHolderViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView ivImgClassAttendanceHolder;
        public TextView classAttendanceHoldersName;
        public TextView classAttendanceHoldersInfo;

        ItemClickListener itemClickListener;

        public ClassAttendanceHolderViewHolder(View itemView) {
            super(itemView);
            ivImgClassAttendanceHolder = (ImageView) itemView.findViewById(R.id.ivImgAttendence);
            classAttendanceHoldersName = (TextView) itemView.findViewById(R.id.attendanceName);
            classAttendanceHoldersInfo = (TextView) itemView.findViewById(R.id.attendanceCode);
        }

        @Override
        public void onClick(View v) {
            this.itemClickListener.onItemClick(v, getLayoutPosition());
        }

        public void setItemClickListener(ItemClickListener ic) {
            this.itemClickListener = ic;
        }
    }


}
