package jk3634group.tutez.adapter;

import android.widget.Filter;

import java.util.ArrayList;

import jk3634group.tutez.adapter.StudentAdapter;
import jk3634group.tutez.dto.Student;

/**
 * Created by Dooba on 10/1/2016.
 */
public class CustomFilter extends Filter {

    StudentAdapter adapter;
    ArrayList<Student> filterList;

    public CustomFilter(ArrayList<Student> filterList, StudentAdapter adapter){
        this.adapter = adapter;
        this.filterList = filterList;
    }

    //Now to filer results
    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results = new FilterResults();
        //check validity

        if(constraint != null && constraint.length() > 0){
            //change to uppercase
            constraint = constraint.toString().toUpperCase();


            //store filteredstudents
            ArrayList<Student> filteredStudents = new ArrayList<>();

            for(int i = 0; i < filterList.size(); i++){
                //check
                if(filterList.get(i).getFirstName().toUpperCase().contains(constraint) || filterList.get(i).getLastName().toUpperCase().contains(constraint)){
                    filteredStudents.add(filterList.get(i));
                }
            }
            results.count=filteredStudents.size();
            results.values=filteredStudents;
        } else {
            results.count=filterList.size();
            results.values=filterList;
        }

        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        adapter.students=  (ArrayList<Student>) results.values;
        //refresh
        adapter.notifyDataSetChanged();

    }
}
