package jk3634group.tutez.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import jk3634group.tutez.R;
import jk3634group.tutez.dao.ResultsDAO;
import jk3634group.tutez.dto.AssignmentClass;
import jk3634group.tutez.dto.ClassAttendanceHolder;
import jk3634group.tutez.dto.Results;

//This activity provides a form for the user to add a class into the system.
//it used if statements to validate the data that is entered
public class AddResult extends AppCompatActivity {

    //Define the elements of the add student page
    private EditText getClassCode;
    private Button submit;

    //This class will utilise the StudentDAO to add a student to the database
    //By nature of tis design it can add several students in one sitting, as it refreshes after every submission
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_add_result);


            Intent intent = getIntent();
            Bundle extras = intent.getExtras();
            final String assignmentId = extras.getString("ASID");
            final String studentId = extras.getString("STID");

            //Define each element
            getClassCode = (EditText) findViewById(R.id.classCode);
            submit = (Button) findViewById(R.id.buttonSendResult);


            //Execute this code when the button 'submit' is clicked
            submit.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    //get the values from the form
                    addData(Integer.parseInt(assignmentId), Integer.parseInt(studentId), Double.parseDouble(getClassCode.getText().toString()));

                }


            });
        }





    public void addData(int AssignmentId, int student, Double mark){
        //Now we can create a student object
       Results results = new Results();
        results.setStudentId(student);
        results.setAssignmentId(AssignmentId);
        results.setMark(mark);

        ResultsDAO resultsDao = new ResultsDAO(getApplicationContext());
        resultsDao.add(results);


    }




}
