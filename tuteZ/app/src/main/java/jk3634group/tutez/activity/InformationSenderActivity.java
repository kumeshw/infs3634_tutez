package jk3634group.tutez.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.view.MenuItem;
import android.widget.Toast;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import jk3634group.tutez.R;
import jk3634group.tutez.dao.ClassDAO;
import jk3634group.tutez.dao.EnrolmentDAO;
import jk3634group.tutez.dao.InformationSenderDAO;
import jk3634group.tutez.dao.StudentDAO;
import jk3634group.tutez.dto.Class;

import jk3634group.tutez.InformationSendingService.*;
import jk3634group.tutez.dto.Enrolment;
import jk3634group.tutez.dto.Student;

//this class leverages DAO's and the emailSender to send emails to students
//This class is responsible for taking the tutors imputs and
//leveraging the DAOs to access the most appropriate students emails
//and then to send them the information specified
public class InformationSenderActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    //Define the action toggle bar
    ActionBarDrawerToggle toggle;
    private Spinner classSpinner;
    private EditText sendMessage;
    private EditText sendSubject;
    private Button button;

    //Main method to run the project
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information_sender);

        //Define the drawer layout
        DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout_notify);
        toggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close);


        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NavigationView navigationView = (NavigationView) findViewById(R.id.navigationView9);
        navigationView.setNavigationItemSelectedListener(this);

        //Set up the form
        classSpinner = (Spinner) findViewById(R.id.classSpinner);

        //Create a data access object
        final ClassDAO classDAO = new ClassDAO(getApplicationContext());
        final StudentDAO studentDAO = new StudentDAO(getApplicationContext());
        //Generate list of classes from said object
        ArrayList<Class> classes = null;
        classes = classDAO.getAllClasses();

        //List of strings for spinner object
        List<String> spinnerList = new ArrayList<String>();

        //Fill it with class code
        for(Class c: classes){
            spinnerList.add(c.getClassCode());
        }

        //Create a data adapter to match data to simple spinner dropdown layout
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, spinnerList);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        classSpinner.setAdapter(dataAdapter);

        //Denote what item has been selected
        classSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapter, View v,
                                       int position, long id) {
                // On selecting a spinner item
                String classCode = adapter.getItemAtPosition(position).toString();
                // Showing selected spinner item
                Toast.makeText(getApplicationContext(),
                        "Selected Class : " + classCode, Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        //Complete the rest of the form
        sendMessage = (EditText) findViewById(R.id.editTextMessage);
        sendSubject = (EditText) findViewById(R.id.editTextSubject);
        button = (Button) findViewById(R.id.buttonSend);

        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //Execute this code when the information sender form is selected.

                //Get values from the from

                String subject = sendSubject.getText().toString().trim();
                String message = sendMessage.getText().toString().trim();
                String classCode = String.valueOf(classSpinner.getSelectedItem());

                //We need to validate that all information has been submitted
                if(subject == null || subject.equals("") || message == null || message.equals("") ||
                        classSpinner == null ||classSpinner.equals("")) {
                    //Execute this code only when one or more fields is left blank
                    Toast.makeText(getApplicationContext(), "Please fill out all feilds", Toast.LENGTH_LONG).show();
                } else {

                    //Use DAO to get the class from this class code

                    Class classObjCurrent = classDAO.getClasssByClassCode(classCode);

                    //Now get a list of all enrolled students in said class
                    EnrolmentDAO enrolmentDAO = new EnrolmentDAO(getApplicationContext());
                    ArrayList<Enrolment> enrolments = enrolmentDAO.getAllEnrolmentsByClassId(classObjCurrent.getClassID());

                    //Iterate through all the enrolments to get students

                    for (Enrolment e : enrolments) {
                        //Use the studentDAO to create a student from the enrolments studentId
                        int studentId = e.getStudentId();

                        //create student object
                        Student student = studentDAO.getStudentsById(studentId);
                        String studentEmail = student.getEmail();

                        //Get date for email sent date
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                        Date current = new Date();

                        //Create a formatted email using HTML
                        String messageFormatted =
                                "<font face='Arial'>"
                                        + "<table width='600' max-height='100%' border='3'>"
                                        + "<tr>"
                                        + "<td colspan='2' bgcolor=Gold align='center'>"
                                        + "<img style='width: 120px; height:100px' src='https://coursera-university-assets.s3.amazonaws.com/6c/b759685fe8d8dcb491ac80208ca2b7/unsw_logo_360x360.png' align='left'>"
                                        + "<h1> Tutez email notification system</h1>"
                                        + "</td>"
                                        + "</tr>"
                                        + "<tr>"
                                        + "<td colspan='1' bgcolor=Black align='center'>"
                                        + "<font size=2 color='white'>"
                                        + "<p> " + subject + " </p>"
                                        + "</font>"
                                        + "</td>"
                                        + "</tr>"
                                        + "<tr valign='center'>"
                                        + "<td bgcolor='#eee' width='78%' height='78%'>" + "<br />"
                                        + "<p align='right'>" + sdf.format(current) + "</p>"
                                        + "<br />"
                                        + "<p> Dear " + student.getFirstName() + "</p><br />"
                                        + message
                                        + "</p>"
                                        + "<p> Kind Regards, </p>"
                                        + "<p> Tutez ~ tutor </p>"
                                        + "</td>"
                                        + "</tr>"
                                        + "<tr>"
                                        + "<td colspan='2' bgcolor=Black align='left'>"
                                        + "<font size=1 color='white'>"
                                        + "<p> UNSW AUSTRALIA  |  UNSW SYDNEY NSW 2052 AUSTRALIA</p>"
                                        + "</font>"
                                        + "<font size=1 color='white'>"
                                        + "<p> ABN 57 195 873 179  |  CRICOS Provider Code 00098G</p>"
                                        + "</font>"
                                        + "</td>"
                                        + "</tr>"
                                        + "</table>"
                                        + "</font>";

                        //Create an emailSender class
                        emailSender es = new emailSender(InformationSenderActivity.this, studentEmail, subject, messageFormatted);

                        //Execute the async task for emails
                        es.execute();

                    }
                }
            }

        });

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        if(toggle.onOptionsItemSelected(item))
            return(true);
        return super.onOptionsItemSelected(item);
    }


    //This method holds the intents for when menu items are selected
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            //These reference the menu.navmenu xml options
            case R.id.nav_menu_1:
                Intent studentIntent = new Intent(this, StudentMainActivity.class);
                this.startActivity(studentIntent);
                finish();
                break;
            case R.id.nav_menu_2:
                Intent classesIntent = new Intent(this, ClassesMainActivity.class);
                this.startActivity(classesIntent);
                finish();
                break;
            case R.id.nav_menu_3:
                Intent assignmentsIntent = new Intent(this, AssignmentsMainActivity.class);
                this.startActivity(assignmentsIntent);
                finish();
                break;
            case R.id.nav_menu_4:
                Intent notifyIntent = new Intent(this, InformationSenderActivity.class);
                this.startActivity(notifyIntent);
                finish();
                break;
        }

        return false;
    }
}
