package jk3634group.tutez.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;

import jk3634group.tutez.R;
import jk3634group.tutez.Tester;
import jk3634group.tutez.adapter.ClassAdapter;
import jk3634group.tutez.dao.ClassDAO;
import jk3634group.tutez.dto.Class;

/**
 * tuteZ
 * <p>
 * Created by kumeshw on 23/10/2016.
 */

public class ClassesMainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    //Define the action toggle bar
    ActionBarDrawerToggle toggle;
    private RecyclerView classRecyclerView;
    private SearchView searchView;
    private Button button;

    //Main method to run the project
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_classes_main);

        //Define the drawer layout
        DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout3);
        toggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close);


        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NavigationView navigationView = (NavigationView) findViewById(R.id.navigationView3);
        navigationView.setNavigationItemSelectedListener(this);

        //Now for the content
        Context context = getApplicationContext();

        //Create the button for adding a student
        button = (Button) findViewById(R.id.button2);


        //Now we are going to get and do everything necessary for putting student_list_items into students
        //Define the search view
        searchView = (SearchView) findViewById(R.id.mSearchClass);
        //Define the recyclerView
        //-- not student main activity but recycler view
        classRecyclerView = (RecyclerView) findViewById(R.id.my_rec_class);
        classRecyclerView.setHasFixedSize(true);

        //Give it a linear layout manager
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        //set the manager
        classRecyclerView.setLayoutManager(linearLayoutManager);

        //use the DAO to get an arrayList of students
        ClassDAO classDAO = new ClassDAO(context);
        ArrayList<Class> classes = classDAO.getAllClasses();
        //now we need to get a student adaper
        final ClassAdapter adapter = new ClassAdapter(getApplicationContext(), classes);
        classRecyclerView.setAdapter(adapter);

        //For the search -- first name and last name
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener(){

            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                //filter as you type
                adapter.getFilter().filter(query);


                return false;
            }
        });
    }

    //For the button to add class
    public void addClass(View view)
    {
        Intent intent = new Intent(ClassesMainActivity.this, AddClass.class);
        startActivity(intent);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        if(toggle.onOptionsItemSelected(item))
            return(true);
        return super.onOptionsItemSelected(item);
    }


    //This method holds the intents for when menu items are selected
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            //These reference the menu.navmenu xml options
            case R.id.nav_menu_1:
                Intent studentIntent = new Intent(this, StudentMainActivity.class);
                this.startActivity(studentIntent);
                finish();
                break;
            case R.id.nav_menu_2:
                Intent classesIntent = new Intent(this, ClassesMainActivity.class);
                this.startActivity(classesIntent);
                finish();
                break;
            case R.id.nav_menu_3:
                Intent assignmentsIntent = new Intent(this, AssignmentsMainActivity.class);
                this.startActivity(assignmentsIntent);
                finish();
                break;
            case R.id.nav_menu_4:
                Intent notifyIntent = new Intent(this, InformationSenderActivity.class);
                this.startActivity(notifyIntent);
                finish();
                break;
        }

        return false;
    }
}
