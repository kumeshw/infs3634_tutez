package jk3634group.tutez.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import jk3634group.tutez.R;
import jk3634group.tutez.dao.ClassDAO;
import jk3634group.tutez.dao.AssignmentDAO;
import jk3634group.tutez.dto.Class;
import jk3634group.tutez.dto.Assignments;

//This activity provides a form for the user to add a assignment into the system.
//it used if statements to validate the data that is entered
public class AddAssignments extends AppCompatActivity {

    //Define the elements of the add student page
    private EditText getAssignmentName;
    private EditText getAssignmentDescription;
    private Button submit;


    //This class will utilise the DAOSto add an assignment to the database
    //By nature of its design it can add several students in one sitting, as it refreshes after every submission
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_add_assignments);

        //Define each element
        getAssignmentName = (EditText) findViewById(R.id.assignmentName);
        getAssignmentDescription = (EditText) findViewById(R.id.assignmentDescription);
        submit = (Button) findViewById(R.id.buttonSendAssignment);

        //Create a data access object
        final ClassDAO classDAO = new ClassDAO(getApplicationContext());
        //Generate list of classes from said object
        ArrayList<Class> classes = null;
        classes = classDAO.getAllClasses();






        //Execute this code when the button 'submit' is clicked
        submit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                //Create an Assignment object
                String assignmentName = getAssignmentName.getText().toString().trim();
                String assignmentDescripton = getAssignmentDescription.getText().toString().trim();

                //Firstly check to make sure all entries are filled out
                if (assignmentName == null || assignmentName.equals("") || assignmentDescripton == null || assignmentDescripton.equals("")) {
                    //Execute this code only when one or more fields is left blank
                    Toast.makeText(getApplicationContext(), "Please fill out all feilds", Toast.LENGTH_LONG).show();
                } else {




                    //Now we can add values
                    addData(assignmentName, assignmentDescripton);

                    //Clear the form
                    getAssignmentName.setText("");
                    getAssignmentDescription.setText("");


                }


            }


        });

    }


    //Create and submit assignment object
    public void addData(String name, String description){

        //create a class object
        Assignments assignments = new Assignments();
        assignments.setAssignmentName(name);
        assignments.setAssignmentDescription(description);
        //add to database
        AssignmentDAO assignmentDAO = new AssignmentDAO(getApplicationContext());
        assignmentDAO.add(assignments);



    }




}
