package jk3634group.tutez.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.CandleStickChart;
import com.github.mikephil.charting.data.CandleEntry;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import jk3634group.tutez.R;
import jk3634group.tutez.adapter.ClassAdapter;
import jk3634group.tutez.dao.AssignmentClassDAO;
import jk3634group.tutez.dao.AssignmentDAO;
import jk3634group.tutez.dao.ClassDAO;
import jk3634group.tutez.dao.EnrolmentDAO;
import jk3634group.tutez.dao.ResultsDAO;
import jk3634group.tutez.datavisualisation.ClassResults;
import jk3634group.tutez.dto.AssignmentClass;
import jk3634group.tutez.dto.Assignments;
import jk3634group.tutez.dto.Class;
import jk3634group.tutez.dto.ClassAttendence;
import jk3634group.tutez.dto.Results;

//Important to know that the assignment DTO is transferred into this class by
//The Assignment adapter.


public class ClassProfile extends AppCompatActivity {

    //initiate profile values
    private TextView className;
    private TextView classCode;
    private TextView classInfo;
    private Button button;
    private Button button2;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_class_profile);

        //Define the profile values
        className = (TextView) findViewById(R.id.classNameProfile);
        classCode = (TextView) findViewById(R.id.classCodeProfile);
        classInfo = (TextView) findViewById(R.id.classDetails);
        button = (Button) findViewById(R.id.viewClassAttendance);
        button2 = (Button) findViewById(R.id.deleteClass);


        //Determine if the intent was successful
        if (getIntent().getSerializableExtra("class") != null) {

            //Cast the intent to an Assignment and make a new assignment object from it
            final Class classObj = (Class) getIntent().getSerializableExtra("class");

            //fill out the values we can
            className.setText(classObj.getClassName());
            classCode.setText(classObj.getClassCode());
            classInfo.setText("Class held in " + classObj.getYear() + ", Semester " + classObj.getSemester());

            //Code for results
            //Use a candleStick chart
            ResultsDAO resultsDAO = new ResultsDAO(getApplicationContext());
            AssignmentDAO assignmentDAO = new AssignmentDAO(getApplicationContext());
            AssignmentClassDAO assignmentClassDAO = new AssignmentClassDAO(getApplicationContext());
            ArrayList<AssignmentClass> assignmentClasses = assignmentClassDAO.getAllAssignmentClassByClassId(classObj.getClassID());


            ArrayList<CandleEntry> candleEntries = new ArrayList<>();
            //iterate through all the classes assignments
            for (AssignmentClass as : assignmentClasses) {

                Assignments assignments = assignmentDAO.getAssignmentById(as.getAssignmentId());

                ArrayList<Results> resultsFromAssignent = resultsDAO.getResultsByAssignmentId(as.getAssignmentId());
                if(resultsFromAssignent != null) {
                    try {
                        ClassResults classResults = new ClassResults(resultsFromAssignent);
                        if (resultsFromAssignent.size() > 0) {
                            {
                                Float highPercentile = Float.parseFloat(classResults.getHighPercentile().toString());
                                Float lowPercentile = Float.parseFloat(classResults.getLowPercentile().toString());
                                Float max = Float.parseFloat(classResults.getMax().toString());
                                Float min = Float.parseFloat(classResults.getMin().toString());

                            }
                        }
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                }



            }
            //Need to do class attendance
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Create an intent to go to this specific assigments profile
                    Intent in = new Intent(getApplicationContext(), ClassAttendanceHolderActivity.class);

                    //Pass the assignment as an intent extra
                    in.putExtra("class", classObj);
                    in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    getApplicationContext().startActivity(in);

                    Toast.makeText(getApplicationContext(), "Attendance for: " + classObj.getClassName(), Toast.LENGTH_LONG).show();

                }
            });

            button2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ClassDAO classDAO = new ClassDAO(getApplicationContext());
                    classDAO.deleteById(classObj.getClassID());
                    EnrolmentDAO enrolmentDAO = new EnrolmentDAO(getApplicationContext());
                    enrolmentDAO.deleteByClassId(classObj.getClassID());

                    finish();
                }


            });


        } else {
            Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_LONG).show();
        }
    }

}
