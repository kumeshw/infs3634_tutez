package jk3634group.tutez.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import jk3634group.tutez.R;
import jk3634group.tutez.dao.ClassDAO;
import jk3634group.tutez.dto.Class;

//This activity provides a form for the user to add a class into the system.
//it used if statements to validate the data that is entered
public class AddClass extends AppCompatActivity {

    //Define the elements of the add student page
    private EditText getClassCode;
    private EditText getClassName;
    private EditText getYear;
    private Spinner getSemester;
    private Button submit;


    //This class will utilise the StudentDAO to add a student to the database
    //By nature of tis design it can add several students in one sitting, as it refreshes after every submission
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_add_class);

        //Define each element
        getClassCode = (EditText) findViewById(R.id.classCode);
        getClassName = (EditText) findViewById(R.id.className);
        getYear = (EditText) findViewById(R.id.Year);
        getSemester = (Spinner) findViewById(R.id.semesterSpinner);

        //Put appropriate valeus in the spinner
        List<String> spinnerList = new ArrayList<String>();
        spinnerList.add("1");
        spinnerList.add("2");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, spinnerList);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        getSemester.setAdapter(dataAdapter);

        //Set on item selected listner
        getSemester.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapter, View v,
                                       int position, long id) {
                // On selecting a spinner item
                String semester = adapter.getItemAtPosition(position).toString();
                // Showing selected spinner item
                Toast.makeText(getApplicationContext(),
                        "Selected Semester : " + semester, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });


        submit = (Button) findViewById(R.id.buttonSendClass);

        //Execute this code when the button 'submit' is clicked
        submit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                //get the values from the form
                String className = getClassName.getText().toString().trim();
                String classCode = getClassCode.getText().toString().trim();
                int year = 0;
                String yearString = getYear.getText().toString().trim();
                if(yearString ==null || yearString.equals("")) {
                    Toast.makeText(getApplicationContext(), "Please fill out all feilds", Toast.LENGTH_LONG).show();

                }else{
                    year = Integer.parseInt(yearString);
                }
                String semester = String.valueOf(getSemester.getSelectedItem());
                int semesterInt = Integer.parseInt(semester);


                //Firstly check to make sure all entries are filled out
                if (className == null || className.equals("") || classCode == null || classCode.equals("") || semester == null || semester.equals("") || yearString == null || yearString.equals("")) {
                    //Execute this code only when one or more fields is left blank
                    Toast.makeText(getApplicationContext(), "Please fill out all feilds", Toast.LENGTH_LONG).show();
                } else {
                    //Now we can add values
                    addData(className, classCode, year, semesterInt);

                    //Clear the form
                    getClassCode.setText("");
                    getClassName.setText("");
                    getYear.setText("");

                }


            }


        });

    }



    public void addData(String name, String code, int year, int semester){
        //Now we can create a student object
        Class classObj = new Class();
        classObj.setClassName(name);
        classObj.setClassCode(code);
        classObj.setSemester(semester);
        classObj.setYear(year);

        //Now we can use the StudentDAO to add this new student into the database
        ClassDAO studentDAO = new ClassDAO(getApplicationContext());
        studentDAO.add(classObj);


    }




}
