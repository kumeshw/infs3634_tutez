package jk3634group.tutez.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import jk3634group.tutez.R;
import jk3634group.tutez.adapter.ClassAdapter;
import jk3634group.tutez.dao.AssignmentDAO;
import jk3634group.tutez.dao.AssignmentClassDAO;
import jk3634group.tutez.dao.ClassDAO;
import jk3634group.tutez.dao.StudentDAO;
import jk3634group.tutez.dto.Assignments;
import jk3634group.tutez.dto.Enrolment;
import jk3634group.tutez.dto.AssignmentClass;
import jk3634group.tutez.dto.Class;

//Important to know that the assignment DTO is transfered into this class by
//The Assignment adapter.


public class AssignmentProfile extends AppCompatActivity {

    //initiate profile values
    private RecyclerView classesRec;
    private TextView assignmentDesc;
    private TextView assignmentName;
    private TextView assignmentInfo;
    private Spinner assignmentClassSpinner;
    private Button addClass;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_assignment_profile);

        //Define the profile values
        classesRec = (RecyclerView) findViewById(R.id.my_rec_assignmentsProfile);
        classesRec.setHasFixedSize(true);
        assignmentDesc = (TextView) findViewById(R.id.assignmentDescriptionProfile);
        //assignmentInfo = (TextView) findViewById(R.id.assignmentInfoProfile);
        assignmentName = (TextView) findViewById(R.id.assignmentNameProfile);
        assignmentClassSpinner = (Spinner) findViewById(R.id.classAssignmentClass);
        addClass = (Button) findViewById(R.id.assignClass);

        //Determine if the intent was successful
        if (getIntent().getSerializableExtra("assignment") != null) {

            //Cast the intent to an Assignment and make a new assignment object from it
            final Assignments assignment =  (Assignments) getIntent().getSerializableExtra("assignment");
            System.out.println("AssignmentId " + assignment.getAssignmentId());
            //fill out the values we can
            assignmentName.setText(assignment.getAssignmentName());
            assignmentDesc.setText(assignment.getAssignmentDescription());

            //First get all classes available to add
            final ClassDAO classDAO = new ClassDAO(getApplicationContext());

            //Now we will create a RecyclerView of all classes that are assigned to this assignment

            //first we get the items needed
            ArrayList<Class> assignedClasses = new ArrayList<Class>();

            AssignmentClassDAO assignmentClassDAO = new AssignmentClassDAO(getApplicationContext());

            try {
                //Get all assignmentclasses by assignment id
                ArrayList<AssignmentClass> assignmentClasses = assignmentClassDAO.getAllAssignmentClassByAssignmentId(assignment.getAssignmentId());

                //create a new class from each and send them to the assignedClassed array
                for (AssignmentClass as : assignmentClasses) {
                    Class classObj = classDAO.getClasssById(as.getClassId());
                    assignedClasses.add(classObj);
                    System.out.println("AssignmentClass id " + as.getAssignmentClassId());
                    System.out.println("AssignmentClass assid " + as.getAssignmentId());
                }

                //Now we can populate the recycler view
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
                classesRec.setLayoutManager(linearLayoutManager);
                final ClassAdapter adapter = new ClassAdapter(getApplicationContext(), assignedClasses);
                classesRec.setAdapter(adapter);
            } catch(Exception e){
                Toast.makeText(getApplicationContext(), "Assignment has no classes", Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
            //Generate list of classes from said object
            ArrayList<Class> classes = null;
            classes = classDAO.getAllClasses();

            //List of strings for spinner object
            List<String> spinnerList = new ArrayList<String>();

            //Fill it with class code
            for(Class c: classes){
                spinnerList.add(c.getClassCode());
            }

            //Create a data adapter to match data to simple spinner dropdown layout
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_item, spinnerList);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            assignmentClassSpinner.setAdapter(dataAdapter);


            addClass.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Create an intent to go to this specific assignments profile
                    String classCode = String.valueOf(assignmentClassSpinner.getSelectedItem());

                    //Create a class object by classCode
                    Class classObj = classDAO.getClasssByClassCode(classCode);

                    //Create and assignment class
                    AssignmentClass assignmentClass = new AssignmentClass();
                    assignmentClass.setClassId(classObj.getClassID());
                    assignmentClass.setAssignmentId(assignment.getAssignmentId());

                    //create DAO
                    AssignmentClassDAO assignmentClassDAO = new AssignmentClassDAO(getApplicationContext());

                    //Send to the database
                    assignmentClassDAO.addOrUpdate(assignmentClass);


                    Toast.makeText(getApplicationContext(), classObj.getClassName() + " added.", Toast.LENGTH_LONG).show();


                    //Refresh the RecyclerView
                    finish();
                    startActivity(getIntent());

                }
            });



        } else {
            Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_LONG).show();
        }


    }


}
