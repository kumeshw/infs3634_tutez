package jk3634group.tutez.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;

import jk3634group.tutez.R;
import jk3634group.tutez.adapter.ClassAttendanceAdapter;
import jk3634group.tutez.dao.ClassAttendanceHolderDAO;
import jk3634group.tutez.dto.Class;
import jk3634group.tutez.dto.ClassAttendanceHolder;

/**
 * tuteZ
 * <p>
 * Created by homeworld on 24/10/2016.
 */

public class ClassAttendanceHolderActivity extends AppCompatActivity {

    //Define the action toggle bar
    ActionBarDrawerToggle toggle;
    private RecyclerView attendenceRecyclerView;
    private SearchView searchView;
    private Button button;


    //Main method to run the project
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_classattendance_list);

        //Define the drawer layout


        //Now for the assignments stuff
        //Now for the content
        Context context = getApplicationContext();
        if (getIntent().getSerializableExtra("class") != null) {

            //Create class
            final Class classObj =  (Class) getIntent().getSerializableExtra("class");

            //Create the button for adding a student
            button = (Button) findViewById(R.id.button6);
            //Now we are going to get and do everything necessary for putting student_list_items into students
            //Define the search view
            searchView = (SearchView) findViewById(R.id.mSearchAttendance);
            //Define the recyclerView
            //-- not student main activity but recycler view
            attendenceRecyclerView = (RecyclerView) findViewById(R.id.my_rec_attendance);
            attendenceRecyclerView.setHasFixedSize(true);

            //Give it a linear layout manager
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
            //set the manager
            attendenceRecyclerView.setLayoutManager(linearLayoutManager);

            //use the DAO to get an arrayList of students
            //We need to get a list of all the attendance holders.
            ClassAttendanceHolderDAO classAttendanceHolderDAO = new ClassAttendanceHolderDAO(getApplicationContext());

            try {
                ArrayList<ClassAttendanceHolder> attendences = classAttendanceHolderDAO.getAllClassAttendanceByClasssId(classObj.getClassID());


                //now we need to get a student adaper
                final ClassAttendanceAdapter adapter = new ClassAttendanceAdapter(this, attendences);
                attendenceRecyclerView.setAdapter(adapter);

                //For the search -- first name and last name
                searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String query) {
                        //filter as you type
                        adapter.getFilter().filter(query);


                        return false;
                    }
                });


            } catch (Exception e) {
                Toast.makeText(context, "No class attendance forms made", Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }

            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                    //Create an intent to go to this specific assigments profile
                    Intent in = new Intent(getApplicationContext(), AttendanceForm.class);

                    //Pass the assignment as an intent extra
                    in.putExtra("class", classObj);
                    in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    getApplicationContext().startActivity(in);

                    Toast.makeText(getApplicationContext(), "Attendance for: " + classObj.getClassName(), Toast.LENGTH_LONG).show();

                }
            });
        } else {

        }

    }






}
