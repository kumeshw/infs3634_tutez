package jk3634group.tutez.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;
import java.util.List;

import jk3634group.tutez.R;
import jk3634group.tutez.adapter.ClassAdapter;

import jk3634group.tutez.dao.AssignmentClassDAO;
import jk3634group.tutez.dao.AssignmentDAO;
import jk3634group.tutez.dao.EnrolmentDAO;

import jk3634group.tutez.dao.ResultsDAO;
import jk3634group.tutez.dao.StudentDAO;
import jk3634group.tutez.dto.AssignmentClass;
import jk3634group.tutez.dto.Assignments;
import jk3634group.tutez.dto.Results;
import jk3634group.tutez.dto.Student;
import jk3634group.tutez.dto.Enrolment;
import jk3634group.tutez.dao.ClassDAO;
import jk3634group.tutez.dto.Class;

import jk3634group.tutez.datavisualisation.*;

//Important to know that the assignment DTO is transfered into this class by
//The Assignment adapter.


public class StudentProfile extends AppCompatActivity {

    //initiate profile values
    private RecyclerView classesRec;
    private TextView studentName;
    private TextView studentInfo;
    private ListView listView;
    private Spinner studentClassSpinner;
    private Button addClass;
    private ArrayList<Results> results;
    private ArrayList<AssignmentClass> assignedAssignments = new ArrayList<AssignmentClass>();
    BarChart barChart;


    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private com.google.android.gms.common.api.GoogleApiClient client;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_student_profile);

        //Define the profile values
        classesRec = (RecyclerView) findViewById(R.id.my_rec_studentProfile);
        classesRec.setHasFixedSize(true);
        studentInfo = (TextView) findViewById(R.id.studentInfoProfile);
        studentName = (TextView) findViewById(R.id.studentNameProfile);
        studentClassSpinner = (Spinner) findViewById(R.id.classStudentClass);
        addClass = (Button) findViewById(R.id.assignClassStudent);
        listView = (ListView) findViewById(R.id.listViewMain);



        ResultsDAO resultsDao = new ResultsDAO(getApplicationContext());
        final AssignmentDAO assignmentDAO = new AssignmentDAO(getApplicationContext());
        AssignmentClassDAO assignmentClassDAO = new AssignmentClassDAO(getApplicationContext());


        //Determine if the intent was successful
        if (getIntent().getSerializableExtra("student") != null) {

            //Cast the intent to an Assignment and make a new assignment object from it
            final Student student = (Student) getIntent().getSerializableExtra("student");




            //fill out the values we can
            studentName.setText(student.getFirstName() + " " + student.getLastName());
            studentInfo.setText(student.getEmail());

            //First get all classes available to add
            final ClassDAO classDAO = new ClassDAO(getApplicationContext());

            //Now we will create a RecyclerView of all classes that are assigned to this student

            //first we get the items needed
            final ArrayList<Class> assignedClasses = new ArrayList<Class>();

            final EnrolmentDAO enrolmentDAO = new EnrolmentDAO(getApplicationContext());
            try {
                //Get all enrolments by assignment id
                ArrayList<Enrolment> enrolments = enrolmentDAO.getAllEnrolmentsByStudentId(student.getStudentId());

                //create a new class from each and send them to the assignedClassed array
                for (Enrolment e : enrolments) {
                    System.out.println(e.getEnrolmentId() + "enrol");
                    Class classObj = classDAO.getClasssById(e.getClassId());
                    assignedClasses.add(classObj);
                }

                //Now we can populate the recycler view
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
                classesRec.setLayoutManager(linearLayoutManager);
                final ClassAdapter adapter = new ClassAdapter(getApplicationContext(), assignedClasses);
                classesRec.setAdapter(adapter);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), "student has no classes", Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
            results = resultsDao.getResultsByStudentId(student.getStudentId());
            final ArrayList<String> assignmentNames = new ArrayList<String>();

            //used the assigned classes to generate a list of assignemtns
            if(assignedClasses != null ){
                for(Class classObj:assignedClasses){
                    try {
                        assignedAssignments.addAll(assignmentClassDAO.getAllAssignmentClassByClassId(classObj.getClassID()));
                        for(AssignmentClass ac: assignedAssignments){
                            System.out.println("here" +ac.getAssignmentClassId());
                            Assignments assignments = assignmentDAO.getAssignmentById(ac.getAssignmentId());
                            assignmentNames.add(assignments.getAssignmentName());
                        }
                    }catch(Exception e){
                        Toast.makeText(getApplicationContext(), "No assigned classes", Toast.LENGTH_LONG).show();
                    }
                }
            }

            //Now lets make a list of all student assignments
            if(assignmentNames != null) {
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_2, android.R.id.text1, assignmentNames);
                listView.setAdapter(adapter);

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
                {

                    @Override
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {
                        // TODO Auto-generated method stub
                        String name  = assignmentNames.get(position);
                        Assignments assignments = assignmentDAO.getAssignmentByName(name);

                        Intent in = new Intent(getApplicationContext(), AddResult.class);
                        Bundle extras = new Bundle();
                        //Pass the assignment as an intent extra
                        extras.putString("ASID", Integer.toString(assignments.getAssignmentId()));
                        extras.putString("STID", Integer.toString(student.getStudentId()));
                        in.putExtras(extras);
                        in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        getApplicationContext().startActivity(in);



                    }
                });




            }

            //Generate list of classes from said object
            ArrayList<Class> classes = null;
            classes = classDAO.getAllClasses();

            //List of strings for spinner object
            List<String> spinnerList = new ArrayList<String>();

            //Fill it with class code
            for (Class c : classes) {
                spinnerList.add(c.getClassCode());
            }

            if(assignedClasses != null) {
                //Create a data adapter to match data to simple spinner dropdown layout
                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                        android.R.layout.simple_spinner_item, spinnerList);
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                studentClassSpinner.setAdapter(dataAdapter);
            }

            addClass.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Create an intent to go to this specific assignments profile
                    String classCode = String.valueOf(studentClassSpinner.getSelectedItem());

                    //Create a class object by classCode
                    Class classObj = classDAO.getClasssByClassCode(classCode);

                    //Create and enrolment
                    Enrolment enrolment = new Enrolment();
                    enrolment.setClassId(classObj.getClassID());
                    enrolment.setStudentId(student.getStudentId());

                    //Send to the database
                    System.out.println(student.getStudentId());
                    enrolmentDAO.add(enrolment);
                    ArrayList<Enrolment> enrolments = enrolmentDAO.getAllEnrolmentsByStudentId(student.getStudentId());
                    for (Enrolment e : enrolments) {
                        System.out.println(e.getStudentId());
                    }

                    Toast.makeText(getApplicationContext(), classObj.getClassName() + " added.", Toast.LENGTH_LONG).show();


                    //Refresh the RecyclerView
                    finish();
                    startActivity(getIntent());

                }
            });




        } else {
            Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_LONG).show();
        }


        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new com.google.android.gms.common.api.GoogleApiClient.Builder(this).addApi(com.google.android.gms.appindexing.AppIndex.API).build();
    }




    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public com.google.android.gms.appindexing.Action getIndexApiAction() {
        com.google.android.gms.appindexing.Thing object = new com.google.android.gms.appindexing.Thing.Builder()
                .setName("StudentProfile Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new com.google.android.gms.appindexing.Action.Builder(com.google.android.gms.appindexing.Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(com.google.android.gms.appindexing.Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        com.google.android.gms.appindexing.AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        com.google.android.gms.appindexing.AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }




}
