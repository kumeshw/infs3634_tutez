package jk3634group.tutez.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import jk3634group.tutez.R;
import jk3634group.tutez.dao.ClassAttendanceHolderDAO;
import jk3634group.tutez.dao.ClassAttendenceDAO;
import jk3634group.tutez.dao.ClassDAO;
import jk3634group.tutez.dao.EnrolmentDAO;
import jk3634group.tutez.dao.StudentDAO;
import jk3634group.tutez.dto.Class;
import jk3634group.tutez.dto.ClassAttendanceHolder;
import jk3634group.tutez.dto.ClassAttendence;
import jk3634group.tutez.dto.Enrolment;
import jk3634group.tutez.dto.Student;

/**
 * Created by homeworld on 10/24/16.
 */

//This class provides a radio button style form for a tutor to take attendance
public class AttendanceForm extends AppCompatActivity {

    //Define form elements
    private Button button;
    private EditText attendanceRecorder;


    //Define local items
    private int classId;

    //When running
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.attendance_form);

        //Create elements
        attendanceRecorder = (EditText) findViewById(R.id.attendanceRecord);
        button = (Button) findViewById(R.id.buttonSendAttendance);

        //create DAOS
        final ClassAttendenceDAO classAttendenceDAO = new ClassAttendenceDAO(getApplicationContext());
        final ClassAttendanceHolderDAO classAttendanceHolderDAO = new ClassAttendanceHolderDAO(getApplicationContext());
        final StudentDAO studentDAO = new StudentDAO(getApplicationContext());
        final ArrayList<ClassAttendence> finalList = new ArrayList<ClassAttendence>();

        ArrayList<ClassAttendence> classAttendences = null;
        LinearLayout linear=(LinearLayout)findViewById(R.id.AttendanceForm);

        //Firstlt check to see if there are already values for class attendance

        if (getIntent().getSerializableExtra("attendance") != null) {
            final ClassAttendanceHolder classObj = (ClassAttendanceHolder) getIntent().getSerializableExtra("attendance");
            System.out.println(classObj.getAttendanceRecord());
            classId = classObj.getClassId();
            System.out.println(classId);
            System.out.println(classObj.getClassAttendanceHolderId());
            int classObjHolderId = classObj.getClassAttendanceHolderId();



            //Now we need to instantiate the classAttendences arrayList
            classAttendences = classAttendenceDAO.getAttendenceByClassAttendanceHolderID(classObjHolderId);


        } else {

        }
        if (getIntent().getSerializableExtra("class") != null) {
            final Class classObj = (Class) getIntent().getSerializableExtra("class");
            classId = classObj.getClassID();
        } else {

        }

        //Case where form was added by button
        if (classAttendences == null) {

            //Case when there has not been any class attendances

            EnrolmentDAO enrolmentDAO = new EnrolmentDAO(getApplicationContext());

            ArrayList<Enrolment> enrolments = enrolmentDAO.getAllEnrolmentsByClassId(classId);


            //make an unselected list
            for (Enrolment e : enrolments) {
                final Student student = studentDAO.getStudentsById(e.getStudentId());

                //Add checkboxes
                TableRow row = new TableRow(this);
                row.setId(student.getStudentId());
                row.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                CheckBox checkBox = new CheckBox(this);
                    checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
                    {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
                        {
                            if ( isChecked )
                            {
                                ClassAttendence classAttendence = new ClassAttendence();
                                classAttendence.setStudentId(student.getStudentId());
                                classAttendence.setClassId(classId);

                                finalList.add(classAttendence);
                            }

                        }
                    });
                checkBox.setId(student.getStudentId());
                checkBox.setText(student.getFirstName() + " " + student.getLastName());
                row.addView(checkBox);
                linear.addView(row);

            }

        } else {
            //Case when there have been attendances made
            ArrayList<Integer> studentIdsThatArePresent = new ArrayList<Integer>();


            for (ClassAttendence ca : classAttendences) {
                studentIdsThatArePresent.add(ca.getStudentId());


                final Student student = studentDAO.getStudentsById(ca.getStudentId());
                //Add checkBoxes
                TableRow row = new TableRow(this);
                row.setId(student.getStudentId());
                row.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                CheckBox checkBox = new CheckBox(this);
                checkBox.setChecked(true);
                checkBox.setId(student.getStudentId());
                checkBox.setText(student.getFirstName() + " " + student.getLastName());
                row.addView(checkBox);
                linear.addView(row);


            }
            EnrolmentDAO enrolmentDAO = new EnrolmentDAO(getApplicationContext());
            ArrayList<Enrolment> enrolments = enrolmentDAO.getAllEnrolmentsByClassId(classId);
            for(Enrolment e: enrolments){
                final Student student = studentDAO.getStudentsById(e.getStudentId());
                int test = 0;
                for(Integer i: studentIdsThatArePresent){
                    if(e.getStudentId() == i){
                        test = 1;
                    }
                }

                if(test == 0){
                    TableRow row = new TableRow(this);
                    row.setId(student.getStudentId());
                    row.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                    CheckBox checkBox = new CheckBox(this);
                    checkBox.setChecked(false);
                    checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
                    {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
                        {
                            if ( isChecked )
                            {
                                Toast.makeText(AttendanceForm.this, "checked", Toast.LENGTH_SHORT).show();

                                ClassAttendence classAttendence = new ClassAttendence();
                                classAttendence.setStudentId(student.getStudentId());
                                classAttendence.setClassId(classId);

                                finalList.add(classAttendence);
                            }

                        }
                    });
                    checkBox.setId(student.getStudentId());
                    checkBox.setText(student.getFirstName() + " " + student.getLastName());
                    row.addView(checkBox);
                    linear.addView(row);
                }
            }

        }



        //Now wait to see if button is selected
        //I.e. form is submitted
        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                EnrolmentDAO enrolmentDAO = new EnrolmentDAO(getApplicationContext());

                ArrayList<Enrolment> enrolments = enrolmentDAO.getAllEnrolmentsByClassId(classId);
                if (classAttendanceHolderDAO.getClassHolderByAttendanceRecord(attendanceRecorder.getText().toString().trim()) == null) {
                    ClassAttendanceHolder classAttendanceHolder = new ClassAttendanceHolder();
                    classAttendanceHolder.setAttendanceRecord(attendanceRecorder.getText().toString().trim());
                    classAttendanceHolder.setClassId(classId);
                    classAttendanceHolderDAO.add(classAttendanceHolder);
                }

                ClassAttendanceHolder cah = classAttendanceHolderDAO.getClassHolderByAttendanceRecord(attendanceRecorder.getText().toString().trim());
                int classHolderId = cah.getClassAttendanceHolderId();

                for(ClassAttendence ca: finalList){
                    ca.setClassAttendanceHolderId(classHolderId);
                    classAttendenceDAO.add(ca);
                    System.out.println("here");
                }

                finalList.clear();
                finish();


            }


        });
    }





}


