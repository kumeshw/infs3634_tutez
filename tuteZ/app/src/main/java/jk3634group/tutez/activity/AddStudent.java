package jk3634group.tutez.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import jk3634group.tutez.R;
import jk3634group.tutez.dao.StudentDAO;
import jk3634group.tutez.dto.Student;

//This activity provides a form for the user to add a student into the system.
//it used if statements to validate the data that is entered
public class AddStudent extends AppCompatActivity {

    //Define the elements of the add student page
    private EditText getStudentFirstName;
    private EditText getStudentLastName;
    private EditText getStudentEmail;
    private Button submit;


    //This class will utilise the StudentDAO to add a student to the database
    //By nature of tis design it can add several students in one sitting, as it refreshes after every submission
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_add_student);

        //Define each element
        getStudentFirstName = (EditText) findViewById(R.id.studentFirstName);
        getStudentLastName = (EditText) findViewById(R.id.studentLastName);
        getStudentEmail = (EditText) findViewById(R.id.studentEmailAddress);
        submit = (Button) findViewById(R.id.buttonSendStudent);

        //Execute this code when the button 'submit' is clicked
        submit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String studentFirstName = getStudentFirstName.getText().toString().trim();
                String studentLastName = getStudentLastName.getText().toString().trim();
                String studentEmail = getStudentEmail.getText().toString().trim();

                //Firstly check to make sure all entries are filled out
                if(studentEmail == null || studentEmail.equals("") || studentFirstName == null || studentFirstName.equals("") ||
                        studentLastName == null ||studentLastName.equals("")) {
                   //Execute this code only when one or more fields is left blank
                    Toast.makeText(getApplicationContext(), "Please fill out all feilds", Toast.LENGTH_LONG).show();
                } else {
                    //Now we can validate the data
                    //Since there is data to validate
                    if (validateData(studentFirstName, studentLastName, studentEmail) == true) {
                        addData(studentFirstName, studentLastName, studentEmail);
                        //Now we can refresh the form
                        getStudentFirstName.setText(" ");
                        getStudentLastName.setText("");
                        getStudentEmail.setText("");
                    } else {
                        Toast.makeText(getApplicationContext(), "Please enter correct values (email must be email@location.com)", Toast.LENGTH_LONG).show();
                    }
                }



            }

        });


    }

    public boolean validateData(String first, String last, String email){
        //In this example, I deemed it not necessary to validate first and last name, since there is already a check to determine
        //if values have been added.

        //Check email
        boolean result = true;
        InternetAddress emailAddr = null;
        try {
            //Validate email
            emailAddr = new InternetAddress(email);
            emailAddr.validate();

        } catch (AddressException e) {
            result = false;
        }
        return result;
    }

    public void addData(String first, String last, String email){
        //Now we can create a student object
        Student student = new Student();
        student.setFirstName(first);
        student.setLastName(last);
        student.setEmail(email);

        //Now we can use the StudentDAO to add this new student into the database
        StudentDAO studentDAO = new StudentDAO(getApplicationContext());
        studentDAO.add(student);


    }




}
