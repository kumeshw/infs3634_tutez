package jk3634group.tutez.InformationSendingService;

import android.os.AsyncTask;

//import necessary files
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

//import java email files
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/*
For reference, the code for this email class was inspired by the tutorial from
https://www.simplifiedcoding.net/android-email-app-using-javamail-api-in-android-studio/
parts of it are the same, parts of it are different
The libraries used are from this tutorial as well
 */


/**
 * Created by homeworld on 10/22/16.
 */

//This class handels everything needed to create and send emails
public class emailSender extends AsyncTask<Void,Void,Void> {

    //Define
    private Context context;
    private Session session;

    //Define values for email
    private String email;
    private String subject;
    private String message;

    //Have a progression dialog for email sending
    private ProgressDialog progressDialog;

    //Define the constructor for the emailSender class
    public emailSender(Context context, String email, String subject, String message){
        //Initializing variables
        this.context = context;
        this.email = email;
        this.subject = subject;
        this.message = message;
    }

    //Copied from the tutorial
    //This class is for the progression dialog during transmission
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        //Showing progress dialog while sending email
        progressDialog = ProgressDialog.show(context,"Sending message","Please wait...",false,false);
    }

    //copied from the tutorial
    //this class is for the progression dialog after transmissions
    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        //Dismissing the progress dialog
        progressDialog.dismiss();
        //Showing a success message
        Toast.makeText(context,"Message Sent",Toast.LENGTH_LONG).show();
    }


    //This is an async task that operates in the background of the application
    //This is where the application sends the email
    //This was copied from the tutorial
    @Override
    protected Void doInBackground(Void... voids) {
        //Creating properties
        Properties props = new Properties();

        //Configuring properties for gmail
        //If you are not using gmail you may need to change the values
        props.put("mail.smtp.host", Config.HOST);
        //not confident about the CONFIG definitions for the ports
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

        //Creating a new session
        session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
                    //Authenticating the password
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(Config.MAILFROM, Config.PASSWORD);
                    }
                });

        try {
            //Creating MimeMessage object
            MimeMessage mm = new MimeMessage(session);

            //Setting sender address
            mm.setFrom(new InternetAddress(Config.MAILFROM));
            //Adding receiver
            mm.addRecipient(Message.RecipientType.TO, new InternetAddress(email));
            //Adding subject
            mm.setSubject(subject);
            //Adding message
            mm.setContent(message, "text/html; charset=utf-8");

            //Sending email
            Transport.send(mm);

        } catch (MessagingException e) {
            e.printStackTrace();
        }
        return null;
    }
}
