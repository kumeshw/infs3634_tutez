package jk3634group.tutez;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import jk3634group.tutez.activity.AssignmentsMainActivity;
import jk3634group.tutez.activity.ClassesMainActivity;
import jk3634group.tutez.activity.InformationSenderActivity;
import jk3634group.tutez.activity.StudentMainActivity;
import jk3634group.tutez.dao.AssignmentClassDAO;
import jk3634group.tutez.dao.AssignmentDAO;
import jk3634group.tutez.dao.ClassAttendanceHolderDAO;
import jk3634group.tutez.dao.ClassAttendenceDAO;
import jk3634group.tutez.dao.ClassDAO;
import jk3634group.tutez.dao.EnrolmentDAO;
import jk3634group.tutez.dao.StudentDAO;
import jk3634group.tutez.dto.ClassAttendanceHolder;
import jk3634group.tutez.dto.ClassAttendence;
import jk3634group.tutez.dto.Enrolment;

//Main activity uses the nav drawer to provide menu style navigation
//This activity serves as the landing page for the app.
public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    /*
    This assignemnt leverages the DAO (Data access object) and DTO (Data transfer object)
    To store and operate with data

    */
    //Define the action toggle bar
    ActionBarDrawerToggle toggle;


    //Main method to run the project
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Define the drawer layout

        DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout_main);
        toggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close);


        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NavigationView navigationView = (NavigationView) findViewById(R.id.navigationView);
        navigationView.setNavigationItemSelectedListener(this);
        /*
        //The deletioning
        AssignmentDAO assignmentDAO = new AssignmentDAO(getApplicationContext());
        assignmentDAO.deleteAll();

        AssignmentClassDAO assignmentClassDAO = new AssignmentClassDAO(getApplicationContext());
        assignmentClassDAO.deleteAll();

        ClassDAO classDAO = new ClassDAO(getApplicationContext());
        classDAO.delete();

        StudentDAO studentDAO = new StudentDAO(getApplicationContext());
        studentDAO.deleteAll();

        EnrolmentDAO enrolmentDAO = new EnrolmentDAO(getApplicationContext());
        enrolmentDAO.deleteAll();

        ClassAttendenceDAO classAttendenceDAO = new ClassAttendenceDAO(getApplicationContext());
        classAttendenceDAO.deleteAll();

        ClassAttendanceHolderDAO classAttendanceHolderDAO = new ClassAttendanceHolderDAO(getApplicationContext());
        classAttendanceHolderDAO.deleteAll();

        */
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        if(toggle.onOptionsItemSelected(item))
            return(true);
        return super.onOptionsItemSelected(item);
    }


    //This method holds the intents for when menu items are selected
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            //These reference the menu.navmenu xml options
            case R.id.nav_menu_1:
                Intent studentIntent = new Intent(this, StudentMainActivity.class);
                this.startActivity(studentIntent);
                finish();
                break;
            case R.id.nav_menu_2:
                Intent classesIntent = new Intent(this, ClassesMainActivity.class);
                this.startActivity(classesIntent);
                finish();
                break;
            case R.id.nav_menu_3:
                Intent assignmentsIntent = new Intent(this, AssignmentsMainActivity.class);
                this.startActivity(assignmentsIntent);
                finish();
                break;
            case R.id.nav_menu_4:
                Intent notifyIntent = new Intent(this, InformationSenderActivity.class);
                this.startActivity(notifyIntent);
                finish();
                break;
        }

        return false;
    }
}
