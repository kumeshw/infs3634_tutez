package jk3634group.tutez.datavisualisation;

import android.content.Context;

import java.util.ArrayList;

import jk3634group.tutez.dao.AssignmentClassDAO;
import jk3634group.tutez.dao.AssignmentDAO;
import jk3634group.tutez.dto.Assignments;
import jk3634group.tutez.dto.Results;
import jk3634group.tutez.dto.Student;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

/**
 * Created by homeworld on 10/24/16.
 */


//this class is used to make a bar graph for all the results the student has achieved
//The assumption is that students results are a % value of 100, thus a value of 40 reflects 40%
public class StudentResults {

    //Define variables
    private ArrayList<Results> results;
    private int size;
    private Context context;
    private ArrayList<BarEntry> barEntries = new ArrayList<>();
    private ArrayList<String> components = new ArrayList<>();


    //Constructor method
    public StudentResults(ArrayList<Results> results, Context context){
        this.results = results;
        size = results.size();
        this.context = context;
    }

    //firstly the method to use the other two methods
    public void createEntries(){

        //iterate through all the results recieved


        int counter = 0;
        for(Results r: results){

            //Firstly we create a bar entry
            Double mark = r.getMark();
            barEntries.add(getBarEntry(mark, counter));
            components.add(getAssignmentName(r.getAssignmentId()));

            counter++;
        }


    }

    //return the assignment name as an x value
    public String getAssignmentName(int id){
        AssignmentDAO assignmentDAO = new AssignmentDAO(context);
        Assignments assignment = assignmentDAO.getAssignmentById(id);
        return assignment.getAssignmentName();

    }

    //Create a bar entry from a mark, counter
    public BarEntry getBarEntry(Double mark, int counter){
        String markString = mark.toString();
        Float markFloat = Float.parseFloat(markString);
       return new BarEntry(markFloat, counter);
    }

    //Return the newly defined bar entries
    public ArrayList<BarEntry> getBarEntries(){
        return this.barEntries;
    }

    //reutrn the newly defined values
    public ArrayList<String> getComponents(){
        return this.components;
    }


}
