package jk3634group.tutez.datavisualisation;

import java.util.ArrayList;
import java.util.Collections;

import jk3634group.tutez.dto.Results;


/**
 * Created by homeworld on 10/24/16.
 */


//this class intends to provide a class profile with the arraylists needed to visually represent the
//marks across each assignment assigned to the class
public class ClassResults {

    //This class attempts to make candleStick graphs such that
    //for each assignment a class has been assigned, it can visualise a
    //Candle stick denoting the top, bottom and average scores it had
    // such as      |--|||||||||------|

    //Firstly define the private variables this class needs
    private ArrayList<Results> results;
    private ArrayList<Double> resultsMarks;
    private Double max;
    private Double min;
    private Double highPercentile;
    private Double lowPercentile;


    //Constructor
    public ClassResults(ArrayList<Results> results){
        this.results = results;
        for(Results r: results){
            resultsMarks.add(r.getMark());
        }
    }

    //Controller method
    public void controller(){
        max = getLargest(resultsMarks);
        min = getSmallest(resultsMarks);
        highPercentile = getPercentile(75, resultsMarks);
        lowPercentile = getPercentile(25, resultsMarks);
    }


    //Now we need to get the max number, the minimum number
    public Double getLargest(ArrayList<Double> resultsMarks){
        Double max = Collections.max(resultsMarks);
        return max;
    }

    public Double getSmallest(ArrayList<Double> resultsMarks){
        Double min = Collections.min(resultsMarks);
        return min;
    }

    //Now we need to get the 75 percentile and the 25 percentile
    //Thanks to http://stackoverflow.com/questions/24048879/how-can-i-calculate-the-nth-percentile-from-an-array-of-doubles-in-php | Mark M

    public Double getPercentile(int percentile, ArrayList<Double> resultsMarks){
        Collections.sort(resultsMarks);

        //Get an index for this percentile
        int index = (percentile/100) * resultsMarks.size();
        Double result = null;
        //Check if w
        if(Math.floor(index) == index){
            result = (resultsMarks.get(index-1) + resultsMarks.get(index) )/2;
        } else{
            result = resultsMarks.get((int) Math.floor(index));
        }

        return result;

    }

    //Getters
    public Double getMax(){
        return this.max;
    }

    public Double getMin(){
        return this.min;
    }

    public Double getHighPercentile(){
        return this.highPercentile;
    }

    public Double getLowPercentile(){
        return this.lowPercentile;
    }


}
